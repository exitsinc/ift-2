<?php include('include/main_header.php'); ?>
<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1>International Foundation for Tolerance</h1>
      </div>
    </div>
  </div>
</div>
<section class="conference-banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="conference-BImg">
          <!-- <img src="images/homeengban.jpeg" alt=""> -->
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <table class="paperTable" style="width:100%;text-align:center">
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>Download</th>
          </tr>
          <tr>
            <td>1</td>
            <td>HE Sheikh Engineer Salem bin Sultan Al Qassimi</td>
            <td><a href="public/papers/HE_Sheikh_Salem_opening_speach.pdf" download class="btn btn-primary">Download</a></td>
          </tr>
          <tr>
            <td>2</td>
            <td>How Neuroscience Can Contribute to the application of Intercultural tolerance, empathy and compassion. <br>
              Dr. Mai Nguyen-Phuong</td>
            <td><a href="public/papers/01.pdf" download class="btn btn-primary">Download</a></td>
          </tr>
          <tr>
            <td>3</td>
            <td>Best Practices & Cases of Tolerance. <br>
              Mr. Sadeque Hussain</td>
            <td><a href="public/papers/02.pdf" download class="btn btn-primary">Download</a></td>
          </tr>
          <tr>
            <td>4</td>
            <td>Integrating the Muslim Community in France: A Personal Experience. <br>
              Dr. Robert Crane </td>
            <td></td>
          </tr>
          <tr>
            <td>5</td>
            <td>Tolerance and Development in the Continent of Africa <br>
              Umoren Victoria Aniesineno
            </td>
            <td><a href="public/papers/04.pdf" download class="btn btn-primary">Download</a></td>
          </tr>
          <tr>
            <td>6</td>
            <td>Tolerance in Sports: An Overview… <br>
              Dr. Melfi Alrasheedi</td>
            <td><a href="public/papers/05.pdf" download class="btn btn-primary">Download</a></td>
          </tr>
          <tr>
            <td>7</td>
            <td>Intolerance and the American Character <br>
              Dr. King V. Cheek </td>
            <td><a href="public/papers/06.pdf" download class="btn btn-primary">Download</a></td>
          </tr>
          <tr>
            <td>8</td>
            <td>Three challenges of an uncertain future <br>
              Dr. George Simons </td>
            <td><a href="public/papers/07.pdf" download class="btn btn-primary">Download</a></td>
          </tr>
          <tr>
            <td>9</td>
            <td>Tolerance in the Media <br>
              Professor Dr. Anil Kumar Srivastav</td>
            <td><a href="public/papers/08.pdf" download class="btn btn-primary">Download</a></td>
          </tr>
          <tr>
            <td>10</td>
            <td>Emerging cyber security threats and trends, 2021 and beyond <br>
              Dr. Earl Johnson </td>
            <td><a href="public/papers/09.pdf" download class="btn btn-primary">Download</a></td>
          </tr>
          <tr>
            <td>11</td>
            <td>Intercommunity Violence and its Consequences East of DR Congo <br>
              Paul Bualambo Seseti
            </td>
            <td><a href="public/papers/10.pdf" download class="btn btn-primary">Download</a></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</section>
<!-- <section class="conference-banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="conference-BImg">
          <img src="images/Education English.jpg" alt="">
        </div>
      </div>
    </div>
  </div>
</section> -->
<!-- <section class="conference-banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="conference-BImg">
          <img src="images/Sports English.jpg" alt="">
        </div>
      </div>
    </div>
  </div>
</section> -->
<!-- <section class="home-register">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <a href="https://docs.google.com/forms/d/e/1FAIpQLSe9lbCiIDTzj5Aorakbj5LIZRUttHtWCI1hLUcPRS5Lux3m7Q/viewform?vc=0&c=0&w=1&flr=0">
          <div class="google-reg">
            <img src="images/reg-400x300.png" alt="">
          </div>
        </a>
      </div>
    </div>
  </div>
</section> -->
<!-- <section class="zoom-slider1" style="background: url('./images/DSC0155.jpg') no-repeat;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        
      </div>
    </div>
  </div>
</section> -->
<!-- <div class="site-section section-3">
  <div class="container">
    
    <div class="row">
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 home-service d-flex">
          <a href="https://iftolerance.org/EN/contact-us/" target="_self"></a>
          <img src="./images/CULTER.jpg" alt="">
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 home-service d-flex">
          <a href="https://toleranceconference.org/" target="_self"></a>
          <img src="./images/CONF.jpg" alt="">
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 home-service d-flex">
          <a href="https://iftolerance.org/EN/membership/" target="_self"></a>
          <img src="./images/member1.jpg" alt="">
        </div>
      </div>
      
      
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12  col-md-12 col-lg-12 col-xl-12 mb-4">
        <div class="sep">
          <img src="./images/sep.gif" alt="">
        </div>
      </div>
    </div>
    
  </div>
</div> -->
<!-- <div class="site-section section-1">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 mb-md-4">
        <div class="home-video">
          <iframe class="ifram-class" src="https://www.youtube.com/embed/rWYP0uC-FF0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="px-lg-3">
          <p class="dropcap">SPONSOR WIDGET</p>
          
          
        </div>
      </div>
      
    </div>
  </div>
</div> -->
<!-- <div class="site-section section-1">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 mb-md-4">
        <img src="images/img_sq.jpg" alt="Image" class="img-fluid">
      </div>
      <div class="col-lg-4">
        <div class="px-lg-3">
          <p class="dropcap">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non temporibus ut quasi aspernatur. Alias rerum tempore animi omnis dolores deleniti quibusdam.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem a possimus quae quis nam ducimus saepe. Facere est maiores unde magnam, incidunt saepe repellendus. Officia natus possimus inventore eius soluta!</p>
        </div>
      </div>
      <div class="col-lg-4">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem a possimus quae quis nam ducimus saepe. Facere est maiores unde magnam, incidunt saepe repellendus. Officia natus possimus inventore eius soluta!</p>
        <p>Facere est maiores unde magnam, incidunt saepe repellendus. Officia natus possimus inventore eius soluta</p>
        
      </div>
    </div>
  </div>
</div> -->
<!-- <div class="section-2">
  <div class="container">
    <div class="row no-gutters align-items-stretch align-items-center">
      <div class="col-lg-3 section-title">
        <div class="service-1-title h-100">
          <h2 class="text-white">Featured Practice Area</h2>
          <p>Lorem ipsum dolor sit consectetur adipisicing elit. Voluptatum dignissimos mollitia vitae accusamus eius doloribus.</p>
          <p><a href="#" class="more">View All</a></p>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="service-1 first h-100" style="background-image: url('images/img_1.jpg')">
          <div class="service-1-contents">
            <span class="wrap-icon">
              <span class="flaticon-balance"></span>
            </span>
            <h2>Business Law</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="service-1 h-100" style="background-image: url('images/img_2.jpg')">
          <div class="service-1-contents">
            <span class="wrap-icon">
              <span class="flaticon-law"></span>
            </span>
            <h2>Criminal Law</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="service-1 h-100" style="background-image: url('images/img_3.jpg')">
          <div class="service-1-contents">
            <span class="wrap-icon">
              <span class="flaticon-courthouse"></span>
            </span>
            <h2>Tax Law</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- <div class="site-section section-3">
  <div class="container">
    <div class="row justify-content-center mb-5">
      <div class="col-lg-6 section-title text-center">
        <h2>Areas of Expertise</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 d-flex">
          <div class="service-2-icon mr-3"><span class="flaticon-law"></span></div>
          <div class="service-2-contents">
            <h3>Insurance Matters</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime harum optio doloribus error dolor.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 d-flex">
          <div class="service-2-icon mr-3"><span class="flaticon-balance"></span></div>
          <div class="service-2-contents">
            <h3>Insurance Matters</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime harum optio doloribus error dolor.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 d-flex">
          <div class="service-2-icon mr-3"><span class="flaticon-auction"></span></div>
          <div class="service-2-contents">
            <h3>Insurance Matters</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime harum optio doloribus error dolor.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 d-flex">
          <div class="service-2-icon mr-3"><span class="flaticon-auction"></span></div>
          <div class="service-2-contents">
            <h3>Insurance Matters</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime harum optio doloribus error dolor.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 d-flex">
          <div class="service-2-icon mr-3"><span class="flaticon-courthouse"></span></div>
          <div class="service-2-contents">
            <h3>Insurance Matters</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime harum optio doloribus error dolor.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 d-flex">
          <div class="service-2-icon mr-3"><span class="flaticon-law"></span></div>
          <div class="service-2-contents">
            <h3>Insurance Matters</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime harum optio doloribus error dolor.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- <div class="site-section section-4" style="background-image: url('images/hero_2.jpg')">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 mr-auto mb-5">
        <h2 class="mb-3">Why Clients Choose Us?</h2>
        <p class="mb-4 d-block">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet, non.</p>
        <p><a href="#" class="btn btn-primary py-3 px-4">Free Consultation</a></p>
      </div>
      <div class="col-lg-6">
        <div class="row">
          <div class="col-md-6 mb-4">
            <div class="feature-1 d-flex">
              <span class="feature-1-icon mr-3">
                <span class="flaticon-law"></span>
              </span>
              <div class="feature-1-contents">
                <strong class="number">%89</strong>
                <span class="caption">Successful Case</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 mb-4">
            <div class="feature-1 d-flex">
              <span class="feature-1-icon mr-3">
                <span class="flaticon-law"></span>
              </span>
              <div class="feature-1-contents">
                <strong class="number">421</strong>
                <span class="caption">Trusted Clients</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 mb-4">
            <div class="feature-1 d-flex">
              <span class="feature-1-icon mr-3">
                <span class="flaticon-law"></span>
              </span>
              <div class="feature-1-contents">
                <strong class="number">392</strong>
                <span class="caption">Expert Lawyers</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 mb-4">
            <div class="feature-1 d-flex">
              <span class="feature-1-icon mr-3">
                <span class="flaticon-law"></span>
              </span>
              <div class="feature-1-contents">
                <strong class="number">913</strong>
                <span class="caption">Honors and Awards</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- <div class="site-section section-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 mr-auto">
        <h2 class="mb-4">We Are An International Law Group, Provides High <span class="text-primary">Quality Service</span></h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi, accusantium.</p>
        
        <div class="call-now">
          <span>Call Now For Immediate Assistance</span>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-request">
          <h2>Request a free consultation</h2>
          <form action="#">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Name">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Phone">
            </div>
            <div class="form-group">
              <textarea class="form-control" placeholder="Case Description" id="" cols="30" rows="2"></textarea>
            </div>
            <div class="form-group">
              <input type="submit" class="btn btn-primary" value="Send Request">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- <div class="site-section section-6">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 mr-auto">
        <h2 class="mb-5">Happy <span class="text-primary">Clients Says</span></h2>
        <div class="owl-carousel nonloop-block-4 testimonial-slider">
          <div class="testimony-1">
            <div class="d-flex align-items-center mb-4">
              <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
              <div>
                <h3>John Doe</h3>
                <p>Business Man</p>
              </div>
            </div>
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
            </blockquote>
          </div>
          <div class="testimony-1">
            <div class="d-flex align-items-center mb-4">
              <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
              <div>
                <h3>John Doe</h3>
                <p>Business Man</p>
              </div>
            </div>
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
            </blockquote>
          </div>
          <div class="testimony-1">
            <div class="d-flex align-items-center mb-4">
              <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
              <div>
                <h3>John Doe</h3>
                <p>Business Man</p>
              </div>
            </div>
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
            </blockquote>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        
        <h2 class="mb-5">Frequently <span class="text-primary">Ask Questions</span></h2>
        <div class="border p-3 rounded mb-2">
          <a data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="true" aria-controls="collapse-1" class="accordion-item h5 d-block mb-0">Law assistance to my business</a>
          <div class="collapse show" id="collapse-1">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
        <div class="border p-3 rounded mb-2">
          <a data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4" class="accordion-item h5 d-block mb-0">The newest part of my legislation</a>
          <div class="collapse" id="collapse-4">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
        <div class="border p-3 rounded mb-2">
          <a data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-2" class="accordion-item h5 d-block mb-0">Are you an internationla law?</a>
          <div class="collapse" id="collapse-2">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
        <div class="border p-3 rounded mb-2">
          <a data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-3" class="accordion-item h5 d-block mb-0">How the system works?</a>
          <div class="collapse" id="collapse-3">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div> -->
<!-- <div class="bg-primary" data-aos="fade">
  <div class="container">
    <div class="row">
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-facebook text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-twitter text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-instagram text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-linkedin text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-pinterest text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-youtube text-white"></span></a>
    </div>
  </div>
</div> -->
<?php include('include/main_footer.php'); ?>