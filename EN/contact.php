<?php include('include/main_header.php'); ?>
<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1>Contact Us</h1>
      </div>
    </div>
    </div>
  </div>
<!-- <div class="site-blocks-cover overlay" style="background-image: url('images/gray-painted-background_53876-94041.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center banner-div" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">Contact Us</h1>
        
      </div>
    </div>
  </div>
</div> -->
<div class="site-section" style="padding-bottom: 0;">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <p class="lead">CONTACT US</p>
        <p class="lead">You can communicate by filling out the form or sending an email directly</p>
        
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 col-xl-7 mb-5">
        <form action="../contact_process.php" method="POST" class="contact-form">
          <div class="row form-group">
            <div class="col-md-12 mb-3 mb-md-0">
              <label class="font-weight-bold" for="name">Full Name</label>
              <input type="text" id="name" name="name" class="form-control" placeholder="Full Name">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label class="font-weight-bold" for="email">Email</label>
              <input type="email" id="email" name="email" class="form-control" placeholder="Email Address">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label class="font-weight-bold" for="subject">Subject</label>
              <input type="text" id="subject" name="subject" class="form-control" placeholder="Enter Subject">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label class="font-weight-bold" for="message">Message</label>
              <textarea name="message" id="message" name="message" cols="30" rows="5" class="form-control"
              placeholder="Say hello to us"></textarea>
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <input type="submit" value="Send Message" class="btn btn-primary py-3 px-4">
            </div>
          </div>
        </form>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 ml-auto">
        <div class="p-4 mb-3 bg-white cont">
          <h3 class="h5 text-black mb-3">International Foundation for Tolerance <br>Professional Corporation</h3>
          <!-- <p class="mb-0 font-weight-bold text-black">International Foundation for Tolerance <br>Professional Corporation</p> -->
          <p class="mb-4 text-black">3911 Concord Pike #8030 SMB #6370 <br>Wilmington, DE 19803, USA</p>
          <hr>
          <h3 class="h5 text-black mb-3">European Office <br> International Foundation for Tolerance</h3>
          <p class="mb-4 text-black">L. BEETHOVEN n ° 2 <br>Anguillara Sabazia <br>Rome 00061, Italy</p>
         
          <p class="mb-0 font-weight-bold text-black">Email Address</p>
          <p class="mb-0"><a href="#">info@iftolerance.org</a></p>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="home-register pt-5 ">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center" style="border: 2px solid;
    border-radius: 15px;
    margin: 0 auto;
    ">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
       <a href="https://docs.google.com/forms/d/e/1FAIpQLSe9lbCiIDTzj5Aorakbj5LIZRUttHtWCI1hLUcPRS5Lux3m7Q/viewform?vc=0&c=0&w=1&flr=0">
        <div class="google-reg">
          <img src="images/reg-400x300.png" alt="">
        </div>
      </a>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                  
                  <div>
                    
                    <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                      <input type="hidden" name="cmd" value="_s-xclick">
                      <input type="hidden" name="hosted_button_id" value="QNMALURH5G3LL">
                      <table>
                        <tr><td><input type="hidden" name="on0" value="Certificates شهادات">Certificates  </td></tr><tr><td><select name="os0">
                        
                        <option value="Asian Tolerance">Asian Tolerance $25.00 USD</option>
                        <option value="Tolerance in Sports">Tolerance in Sports $30.00 USD</option>
                        <option value="Tolerance in Education">Tolerance in Education $30.00 USD</option>
                      </select> </td></tr>
                    </table>
                    <input type="hidden" name="currency_code" value="USD">
                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                  </form>
                </div>
              </div>
  </div>
</div>
</section>


<?php include('include/main_footer.php'); ?>