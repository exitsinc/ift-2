<?php include('include/main_header.php'); ?>
<div class="site-blocks-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-8 text-center banner-div" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">The International University of Tolerance </h1>
        <!-- <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium facere officiis in quo, corporis quasi.</p>
        <p><a href="#" class="btn btn-primary px-4 py-3">Get Started</a></p> -->
      </div>
    </div>
  </div>
</div>
<section class="eduction-main">
	<div class="container">
		<div class="row edu-block">
			<div class="col-md-6" style="text-align: justify;">
				<p>The International University of Tolerance is a non-traditional, non-academic institution and not for profit professional organization dedicated to build bridges of understanding, respect and acceptance among people, cultures and societies through providing learning opportunities across the economic and social boundaries. It provides hands on learning content.</p>
				<p>The International University of Tolerance IUOT is a result of a partnership between The International Foundation for Tolerance (USA based) and the International Organization for Tolerance (UK based).IUOT is the educational arm of these two organizations offering a variety of educational and training programs around the globe. Its programs include certificates and diploma courses.</p>
				<p>The International University of Tolerance offers programs and courses in tolerance, cultural diversity, interfaith communication, education, social behaviour, corporate culture, sports etc. We design learning programs based on requests from schools, government and the corporate world.</p>
			</div>	
			<div class="col-md-6">
				<img src="images/edu.jpg" class="d-block w-100">
			</div>	
		</div>
	</div>
</section>
<section class="programs">
	<div class="container">
		<div class="program-heading">
			<h1>The programs that will be offered in 2020 include:</h1>
		</div>
		<div class="offered">
			<div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Certified Teacher of Tolerance
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
      	<div class="row"> 
      		<div class="col-md-4">
      			<img src="images/iut.jpg" class="d-block w-100">
      		</div>
      	<div class="col-md-8">
      		<p>What they have that you don’t is tolerance. “Tolerance” is the ability to sustain a high-level of strength for several moves on a climb. … The problem most climbers run into is not in figuring out the exercises, but rather how to implement the exercises effectively within the framework of a training program.</p>
      		<button type="button" href="#" class="btn btn-secondary">Download</button>
      	</div>	 
      </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Certified Trainer of Tolerance
        </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        <div class="row"> 
      		<div class="col-md-4">
      			<img src="images/iut.jpg" class="d-block w-100">
      		</div>
      	<div class="col-md-8">
      		<p>What they have that you don’t is tolerance. “Tolerance” is the ability to sustain a high-level of strength for several moves on a climb. … The problem most climbers run into is not in figuring out the exercises, but rather how to implement the exercises effectively within the framework of a training program.</p>
      		<button type="button" href="#" class="btn btn-secondary">Download</button>
      	</div>	 
      </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Certified Manager of Tolerance Unit
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <div class="row"> 
      		<div class="col-md-4">
      			<img src="images/iut.jpg" class="d-block w-100">
      		</div>
      	<div class="col-md-8">
      		<p>What they have that you don’t is tolerance. “Tolerance” is the ability to sustain a high-level of strength for several moves on a climb. … The problem most climbers run into is not in figuring out the exercises, but rather how to implement the exercises effectively within the framework of a training program.</p>
      		<button type="button" href="#" class="btn btn-secondary">Download</button>
      	</div>	 
      </div>
      </div>
    </div>
  </div>
    <div class="card">
    <div class="card-header" id="headingFour">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseTwo">
          Certified Counsultant/Coach of Tolerance
        </button>
      </h2>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
      <div class="card-body">
        <div class="row"> 
      		<div class="col-md-4">
      			<img src="images/iut.jpg" class="d-block w-100">
      		</div>
      	<div class="col-md-8">
      		<p>What they have that you don’t is tolerance. “Tolerance” is the ability to sustain a high-level of strength for several moves on a climb. … The problem most climbers run into is not in figuring out the exercises, but rather how to implement the exercises effectively within the framework of a training program.</p>
      		<button type="button" href="#" class="btn btn-secondary">Download</button>
      	</div>	 
      </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFour">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
          Manniging Cultural differnces
        </button>
      </h2>
    </div>
    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
      <div class="card-body">
        <div class="row"> 
      		<div class="col-md-4">
      			<img src="images/iut.jpg" class="d-block w-100">
      		</div>
      	<div class="col-md-8">
      		<p>What they have that you don’t is tolerance. “Tolerance” is the ability to sustain a high-level of strength for several moves on a climb. … The problem most climbers run into is not in figuring out the exercises, but rather how to implement the exercises effectively within the framework of a training program.</p>
      		<button type="button" href="#" class="btn btn-secondary">Download</button>
      	</div>	 
      </div>
      </div>
    </div>
  </div>
    <div class="card">
    <div class="card-header" id="headingSix">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseTwo">
         Interfaith Communication Certificate
        </button>
      </h2>
    </div>
    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
      <div class="card-body">
        <div class="row"> 
      		<div class="col-md-4">
      			<img src="images/iut.jpg" class="d-block w-100">
      		</div>
      	<div class="col-md-8">
      		<p>What they have that you don’t is tolerance. “Tolerance” is the ability to sustain a high-level of strength for several moves on a climb. … The problem most climbers run into is not in figuring out the exercises, but rather how to implement the exercises effectively within the framework of a training program.</p>
      		<button type="button" href="#" class="btn btn-secondary">Download</button>
      	</div>	 
      </div>
      </div>
    </div>
  </div>
    <div class="card">
    <div class="card-header" id="headingSeven">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseTwo">
          Certificate of Tolerance in Sports
        </button>
      </h2>
    </div>
    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
      <div class="card-body">
        <div class="row"> 
      		<div class="col-md-4">
      			<img src="images/iut.jpg" class="d-block w-100">
      		</div>
      	<div class="col-md-8">
      		<p>What they have that you don’t is tolerance. “Tolerance” is the ability to sustain a high-level of strength for several moves on a climb. … The problem most climbers run into is not in figuring out the exercises, but rather how to implement the exercises effectively within the framework of a training program.</p>
      		<button type="button" href="#" class="btn btn-secondary">Download</button>
      	</div>	 
      </div>
      </div>
    </div>
  </div>
    <div class="card">
    <div class="card-header" id="headingEight">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseTwo">
          Certificate of Tolerance in Media
        </button>
      </h2>
    </div>
    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
      <div class="card-body">
        <div class="row"> 
      		<div class="col-md-4">
      			<img src="images/iut.jpg" class="d-block w-100">
      		</div>
      	<div class="col-md-8">
      		<p>What they have that you don’t is tolerance. “Tolerance” is the ability to sustain a high-level of strength for several moves on a climb. … The problem most climbers run into is not in figuring out the exercises, but rather how to implement the exercises effectively within the framework of a training program.</p>
      		<button type="button" href="#" class="btn btn-secondary">Download</button>
      	</div>	 
      </div>
      </div>
    </div>
  </div>

</div>
		</div>
	</div>
</section>
<section class="edutext edu-block">
	<div class="container edu-con">
		<div class="row ">
			<ul>
				<li>In order to reach the maximum number of learners, the International University of Tolerance will make all its programs available online and through distance learning.</li>
				<li>For information, please click on the program to download the full details.</li>
				<li>For more information please email us at university@iftolerance.org</li>
			</ul>
		</div>
	</div>
</section>
 
 <?php include('include/main_footer.php'); ?>