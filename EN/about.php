<?php include('include/main_header.php'); ?>
<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1>About Us</h1>
      </div>
    </div>
    </div>
  </div>
<!-- <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">About Us</h1>
      </div>
    </div>
  </div>
</div> -->
<!-- mission -->
<div class="site-section section-1 section-1-about bg-light" id="mission-id">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-md-4 section-title">
        <h2 class="text-center">MISSION</h2>
        <p>Our mission is to build bridges of understanding and acceptance among different people, Groups and cultures through research and scientific approaches for the betterment of humankind.</p>
      </div>
      
    </div>
  </div>
</div>
<section>
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <div class="vm-img">
        <img src="images/shutterstock_uae_dec16.jpg" alt="">
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Vision -->
<div class="site-section section-1 section-1-about bg-light" id="vision">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-md-4 section-title">
        <h2 class="text-center">Vision</h2>
        <p>To be the primary center of choice for providing professional advisory services on cultural conflict and for building sustainable bridges among diversified cultures</p>
      </div>

      
    </div>
  </div>
</div>
<!-- board of trustee -->
<div class="site-section section-6 advisory-board-section" id="board-of-trustees">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2>BOARD OF TRUSTEES</h2>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/HE-Sheikh-Eng.-Salem.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>His Excellency Engineer Sheikh Salem Bin Sultan Al Qasimi</h3>
          <p>Chairman,<br/>
          Ras Al Khaima Civil Aviation</p>
          <p>United Arab Emirates</p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/King.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>Dr. King V. Cheek</h3>
          <p>Chairman of the Board of Trustees<br/>
          International Foundation for Tolerance</p>
          <p>New York, USA</p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/joseph 2.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>Bishop Dr. Joseph K. Grieboski</h3>
          <p>President ,<br/> International Center on Religion and Justice
          Washington DC,</p>
          <p>United States of America</p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/Nicholas Cardy 2.jpeg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>Nicholas Cardy</h3>
          <p>International journalist</p>
          <p>United Kingdom</p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/Jaames Chang22.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>Dr. James Chang</h3>
          <p>President <br/> Global Knowledge Exchange Foundation</p>
          <p>United States of America </p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/WhatsApp Image 2020-06-16 at 14.20.27.jpeg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          
          <h3>Dr. Layla Al Bloushi</h3>
          <p>CEO,<br/>
          AlFikrah Management Consulting</p>
          <p>United Arab Emirates </p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/76.jpeg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>Dr. Abdulla Dahlan</h3>
          <p>Chairman<br/>
          University of Business and Technology</p>
          <p>Kingdom of Saudi Arabia</p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/Antonio_Imeneo.jpeg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>Professor Antonio Imeneo</h3>
          <p>Director<br/>
          International Research Center FUNVIC EUROPA</p>
          <p>Rome, Italy</p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/boardoftrusties.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>Mohamed Ali Al Naqi</h3>
          <p>Chairman<br/>
          Kuwait Industrial Company Holding</p>
          <p>Kuwait</p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/TaqiAlZeera.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>Dr. Taqi Al Zeera</h3>
          <p>Founding President<br/>
          Arab Foundation for Supporting Sustainable Development</p>
          <p>Bahrain</p>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- advisory board -->
<div class="site-section section-6 advisory-board-section" id="advisory-board">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2>ADVISORY BOARD</h2>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/george_simons.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>Dr. George Simons</h3>
          <p>CEO, Diversophy</p>
          <p>France</p>
         <!--  <a href="javascript:void();">CEO, Diversophy</a> -->
          <!-- <blockquote>
            <p>Sandra M. Fowler entered the intercultural field in the 1970s researching the Peace Corps PRIST induction program for the National Bureau of Standards Applied Psychology division, followed by 8 years of U.S. Navy personnel research during which the simulation game BaFa BaFa was developed. She directed the Navy’s Overseas Duty Support Program from 1979 to 1989, was president of SIETAR International in 1988, followed by 20 years as an independent consultant and trainer. Her publications include the 2-volume Intercultural Sourcebook of Cross-Cultural Training plus many articles and chapters in a variety of books on intercultural issues.</p>
          </blockquote> -->
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/Robert Crane 2.png" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3>Dr. Robert Crane</h3>
          <p>International scholar</p>
          <p>France</p>
          
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/DianneDetectClose.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3>Dianne Hofner Saphiere</h3>
          <p>President<br/>
          Cultural Detective</p>
          <p>USA</p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/earl johnson.png" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3>Dr. Earl Johnson</h3>
          <a href="javascript:void();">President<br/>ICI USA</a>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/Daisy-Khan.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3>Daisy Khan</h3>
          <p>Founder and Executive Director</p>
          <p>Women’s Islamic initiative in Spirituality and Equality</p>
          <p>USA</p>
          
        </div>
      </div>
       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/Anil.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3>Professor Dr. Anil Srivastav</h3>
          <p>Chairman</p>
          <p>North Eastern India-ASEAN Chamber of Commerce & Industry</p>
          <p>India </p>
         
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/Susan leather blazer 2.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3>SUSAN ALLAN</h3>
          <p>Founder of The Marriage Forum</p>
          <p>USA </p>
         
        </div>
      </div>

       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/Indunil 2.jpeg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3>Indunil Fernando </h3>
          <p>Chief Executive Officer of Veterinary Hospital  </p>
          <p>Sri Lanka </p>
        </div>
      </div>
     
    </div>
  </div>
</div>

<?php include('include/main_footer.php'); ?>