<?php include('include/main_header.php'); ?>
<!-- <section class="reg-ban">
  <div class="regi-ban">
    <img src="https://ctusms.com/wp-content/uploads/2019/01/registration-banner-1200x400.jpg" class="d-block w-100" alt="...">
  </div>
</section>
-->
<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1>Registration</h1>
      </div>
    </div>
  </div>
</div>
<!-- <div class="site-blocks-cover overlay" style="background-image: url('images/gray-painted-background_53876-94041.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center banner-div" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">Registration</h1>
        
      </div>
    </div>
  </div>
</div> -->
<section class="regi-main mtb">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="regdata">
          <div class="jumbotron jumbotron-fluid">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                  <h1 class="display-4">Register for the Asian Regional Tolerance Across Cultures Conference</h1>
                  <hr>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                  <!-- <a href="https://docs.google.com/forms/d/1VZ5oVuZY25I8QOh2vC02J5IxbOzG9S-G8gU5F5Bx3Ok/edit">
                    <div class="google-reg aregistration">
                      <img src="images/reg-400x300.png" alt="">
                    </div>
                  </a> -->
                  <div class="">
                  <h1>PREVENTION OF INDIRECT MUSCLE INJURIES IN SPORT & INTEGRATION</h1>
                    <h1><a href="public/confrences/PreventionofSportsInjuries.pdf" download class="btn btn-primary">Download Brochure</a></h1>
                  </div>
              <div class="">
              <h1>Register Now</h1>
                <h1><a href="https://docs.google.com/forms/d/1y2JVbA0oyI7oStIZI2zXSxYqqrrLtoO-K1w67CS7GsY/edit?gxids=7628" target="_blank" class="btn btn-primary">Registration Form</a></h1>
            </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                  <p class="lead">To register for the Asian Regional Tolerance Across Cultures Conference, please click here to fill in the form. If you have already registered, please do not register again. </p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                  <p class="lead">If you would like to receive the Certificate of Attendance, you may pay the fee from the payment below </p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                  <div id="smart-button-container">

      <div style="text-align: center;">

<!--         <div style="margin-bottom: 1.25rem;">

          <p>تسجيل في برنامج التسامح في الرياضة </p>

          <select id="item-options"><option value="تسجيل عادي " price="120">تسجيل عادي  - 120 USD</option><option value="تسجيل بخصم خاص " price="100">تسجيل بخصم خاص  - 100 USD</option></select>

          <select style="visibility: hidden" id="quantitySelect"></select>

        </div>

      <div id="paypal-button-container"></div> -->
        <!-- <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">

<input type="hidden" name="cmd" value="_xclick">

<input type="hidden" name="business" value="info@iftolerance.org">

<input type="hidden" name="lc" value="US">

<input type="hidden" name="item_name" value="Registration fees in Prevention of Injuries in Sports Program">

<input type="hidden" name="amount" value="50.00">

<input type="hidden" name="currency_code" value="USD">

<input type="hidden" name="button_subtype" value="services">

<input type="hidden" name="no_note" value="0">

<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHostedGuest">

<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">

<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">

</form> -->
      </div>

    </div>

    <script src="https://www.paypal.com/sdk/js?client-id=sb&currency=USD" data-sdk-integration-source="button-factory"></script>

    <script>

      function initPayPalButton() {

        var shipping = 0;

        var itemOptions = document.querySelector("#smart-button-container #item-options");

    var quantity = parseInt();

    var quantitySelect = document.querySelector("#smart-button-container #quantitySelect");

    if (!isNaN(quantity)) {

      quantitySelect.style.visibility = "visible";

    }

    var orderDescription = 'تسجيل في برنامج التسامح في الرياضة ';

    if(orderDescription === '') {

      orderDescription = 'Item';

    }

    paypal.Buttons({

      style: {

        shape: 'rect',

        color: 'gold',

        layout: 'vertical',

        label: 'paypal',

       

      },

      createOrder: function(data, actions) {

        var selectedItemDescription = itemOptions.options[itemOptions.selectedIndex].value;

        var selectedItemPrice = parseFloat(itemOptions.options[itemOptions.selectedIndex].getAttribute("price"));

        var tax = (0 === 0) ? 0 : (selectedItemPrice * (parseFloat(0)/100));

        if(quantitySelect.options.length > 0) {

          quantity = parseInt(quantitySelect.options[quantitySelect.selectedIndex].value);

        } else {

          quantity = 1;

        }

 

        tax *= quantity;

        tax = Math.round(tax * 100) / 100;

        var priceTotal = quantity * selectedItemPrice + parseFloat(shipping) + tax;

        priceTotal = Math.round(priceTotal * 100) / 100;

        var itemTotalValue = Math.round((selectedItemPrice * quantity) * 100) / 100;

 

        return actions.order.create({

          purchase_units: [{

            description: orderDescription,

            amount: {

              currency_code: 'USD',

              value: priceTotal,

              breakdown: {

                item_total: {

                  currency_code: 'USD',

                  value: itemTotalValue,

                },

                shipping: {

                  currency_code: 'USD',

                  value: shipping,

                },

                tax_total: {

                  currency_code: 'USD',

                  value: tax,

                }

              }

            },

            items: [{

              name: selectedItemDescription,

              unit_amount: {

                currency_code: 'USD',

                value: selectedItemPrice,

              },

              quantity: quantity

            }]

          }]

        });

      },

      onApprove: function(data, actions) {

        return actions.order.capture().then(function(details) {

          alert('Transaction completed by ' + details.payer.name.given_name + '!');

        });

      },

      onError: function(err) {

        console.log(err);

      },

    }).render('#paypal-button-container');

  }

  initPayPalButton();

    </script>
                  <!-- <div>
                    
                    <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                      <input type="hidden" name="cmd" value="_s-xclick">
                      <input type="hidden" name="hosted_button_id" value="QNMALURH5G3LL">
                      <table>
                        <tr><td><input type="hidden" name="on0" value="Certificates شهادات">Certificates  </td></tr><tr><td><select name="os0">
                        
                        <option value="Asian Tolerance">Asian Tolerance $25.00 USD</option>
                        <option value="Tolerance in Sports">Tolerance in Sports $30.00 USD</option>
                        <option value="Tolerance in Education">Tolerance in Education $30.00 USD</option>
                      </select> </td></tr>
                    </table>
                    <input type="hidden" name="currency_code" value="USD">
                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                  </form>
                </div> -->
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
  <!-- 2nd row -->
  <!-- <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="regdata">
        <div class="jumbotron jumbotron-fluid" >
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h1 class="display-4">Register for the Tolerance in Sports and Sports Media</h1>
                <hr>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSfCqDNUnNfbyZCwiXtAFFrXkPgtH2rhW7QCgCEYv62fVnt6WA/viewform">
                  <div class="google-reg aregistration">
                    <img src="images/reg-400x300.png" alt="">
                  </div>
                </a>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                <p class="lead">To register for the Tolerance in Sports and Sports Media, please click here to fill in the form. If you have already registered, please do not register again</p>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                <p class="lead"> If you would like to receive the Certificate of Attendance, you may pay the fee from the payment below</p>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                
                <div>
                  
                  <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="QNMALURH5G3LL">
                    <table>
                      <tr><td><input type="hidden" name="on0" value="Certificates شهادات">Certificates  </td></tr><tr><td><select name="os0">
                      
                      <option value="Asian Tolerance">Asian Tolerance $25.00 USD</option>
                      <option value="Tolerance in Sports">Tolerance in Sports $30.00 USD</option>
                      <option value="Tolerance in Education">Tolerance in Education $30.00 USD</option>
                    </select> </td></tr>
                  </table>
                  <input type="hidden" name="currency_code" value="USD">
                  <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                  <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div> -->

  <!-- 3rd row -->
  <!-- <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="regdata">
        <div class="jumbotron jumbotron-fluid" >
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h1 class="display-4">Register for the Tolerance in Education</h1>
                <hr>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                <a href="https://docs.google.com/forms/d/e/1FAIpQLScIJwKotyECtOdhEp0Gk7lblSdSh7KPjfyEf3RF8LNGz9Dagg/viewform?vc=0&c=0&w=1&flr=0">
                  <div class="google-reg aregistration">
                    <img src="images/reg-400x300.png" alt="">
                  </div>
                </a>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                <p class="lead">To register for the Tolerance in Education, please click here to fill in the form. If you have already registered, please do not register again.</p>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                <p class="lead"> If you would like to receive the Certificate of Attendance, you may pay the fee from the payment below</p>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                
                <div>
                  
                  <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="QNMALURH5G3LL">
                    <table>
                      <tr><td><input type="hidden" name="on0" value="Certificates شهادات">Certificates  </td></tr><tr><td><select name="os0">
                      
                      <option value="Asian Tolerance">Asian Tolerance $25.00 USD</option>
                      <option value="Tolerance in Sports">Tolerance in Sports $30.00 USD</option>
                      <option value="Tolerance in Education">Tolerance in Education $30.00 USD</option>
                    </select> </td></tr>
                  </table>
                  <input type="hidden" name="currency_code" value="USD">
                  <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                  <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div> -->

</div>
</section>
<section class="home-register">
<div class="container">
<div class="row d-flex justify-content-center align-items-center">
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
    
  </div>
</div>
</div>
</section>
<?php include('include/main_footer.php'); ?>