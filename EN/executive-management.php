<?php include('include/main_header.php'); ?>

<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
	<div class="container">
		<div class="row align-items-center justify-content-center">
			<div class="col-md-8 text-center" data-aos="fade-up" data-aos-delay="400">
				<h1 class="mb-4">Executive Management</h1>
			</div>
		</div>
	</div>
</div>



    <div class="section-2">
      <div class="container">
        <div class="row no-gutters align-items-stretch align-items-center">
          <div class="col-lg-4">
            <div class="service-1 first h-100" style="background-image: url('images/img_1.jpg')">
              <div class="service-1-contents">
<!--                 <span class="wrap-icon">
                  <span class="flaticon-balance"></span>
                </span> -->
                <h2>Lorem Ipsum</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="service-1 h-100" style="background-image: url('images/img_2.jpg')">
              <div class="service-1-contents">
               <!--  <span class="wrap-icon">
                  <span class="flaticon-law"></span>
                </span> -->
                <h2>Lorem Ipsum</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="service-1 h-100" style="background-image: url('images/img_3.jpg')">
              <div class="service-1-contents">
                <!-- <span class="wrap-icon">
                  <span class="flaticon-courthouse"></span>
                </span> -->
                <h2>Lorem Ipsum</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="service-1 first h-100" style="background-image: url('images/img_1.jpg')">
              <div class="service-1-contents">
               <!--  <span class="wrap-icon">
                  <span class="flaticon-balance"></span>
                </span> -->
                <h2>Lorem Ipsum</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="service-1 h-100" style="background-image: url('images/img_2.jpg')">
              <div class="service-1-contents">
               <!--  <span class="wrap-icon">
                  <span class="flaticon-law"></span>
                </span> -->
                <h2>Lorem Ipsum</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="service-1 h-100" style="background-image: url('images/img_3.jpg')">
              <div class="service-1-contents">
                <!-- <span class="wrap-icon">
                  <span class="flaticon-courthouse"></span>
                </span> -->
                <h2>Lorem Ipsum</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

<?php include('include/main_footer.php'); ?>