<?php include('include/main_header.php'); ?>


<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4">Tolerance in Sports and Sports Media Conference<br>January 11-12,2021</h1>
      </div>
    </div>
  </div>
</div>




<section class="conference-banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="conference-BImg">
          <!-- <img src="images/Sports English.jpg" alt=""> -->
          <img src="images/Tolerance_and_Sports.jpg">
        </div>
      </div>
    </div>
  </div>
</section>

<section class="sport-conference-logo">
  <div class="container">
    <div class="row sport-logo">
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/logo europe.jpg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 mb-xs-5">
        <div class="sport-logo-BImg">
          <img src="images/WhatsApp Image 2020-09-13 at 21.23.00 (1).jpeg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/rowad 3.png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/logo YUOI.png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/WhatsApp Image 2020-09-18 at 17.10.32 (2).jpeg" alt="">
        </div>
      </div>
    </div>
  </div>
</section>



<section class="conferences-main pt-5">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="jumbotron">
          <!-- <div class="row d-flex justify-content-center">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 d-flex justify-content-center align-items-center">
              <div class="conference-img">
                <img src="images/King.jpg" alt="">
              </div>
            </div>
          </div> -->
          <!-- <h1 class="display-4">Hello, world!</h1> -->
          <div class="row">
            <div class="col-md-8">
              <p class="lead">The International Foundation for Tolerance is pleased to invite you all, to take part in this
unique conference on sports and sports media. The main purpose of this conference and other
conferences we organize is to highlight the importance of building bridges of understanding
and acceptance among different social segments and groups.</p>
            </div> 
            <div class="col-md-4">
              <img src="images/sconf (1).jpg" class="d-block w-100">
            </div> 
          </div>
          <div class="row">
            <div class="col-md-4">
              <img src="images/sconf (2).jpg" class="d-block w-100">
            </div> 
            <div class="col-md-8">
              <p class="lead">Watching sports games is always linked with high emotions among players, coaches, managers
and fans. We have seen many incidents in different stadiums and playgrounds, some of which
were deadly. There is a common saying that sport does not know tolerance at all. This is not
true because the purpose of sports is to strengthen team work and spread love and joy among
players and fans. On the other hand, sports media has been blamed for igniting emotions and
creating pressures on teams. Sport should bring us together, but often sport breeds an
intolerance. Why?</p>
            </div> 
          </div>
          <div class="row">
            <div class="col-md-8">
              <p class="lead">This is what this Conference will address.</p>
          <p class="lead">The papers and presentations in this Conference will address the five key players in sports; the
players, the coaches, the managers, the fans and the media. We have a selected panel of
international speakers and researchers from different parts of the world. The conference will
feature concurrent sessions in Arabic and in English so that to give more choices for
participants to select the session they want.</p>
            </div> 
            <div class="col-md-4">
              <img src="images/sconf (3).jpg" class="d-block w-100">
            </div> 
          </div>


          
          
          
          <p class="lead">There are no registration fees and the conference is open to everybody. However, for those
who may request a certificate of attendance, there will be a small fee for that.</p>
          <!-- <p class="lead">Dr. King V Cheek <br> Conference Chair</p> -->
          <!-- <hr class="my-4">
          <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
          <p class="lead">
            <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
          </p> -->
        </div>
      </div>
    </div>
  </div>
</section>
<section class="home-register pt-5">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <a href="https://docs.google.com/forms/d/e/1FAIpQLSfCqDNUnNfbyZCwiXtAFFrXkPgtH2rhW7QCgCEYv62fVnt6WA/viewform">
        <div class="google-reg">
          <img src="images/reg-400x300.png" alt="">
        </div>
        </a>
      </div>
    </div>
  </div>
</section>
<?php include('include/main_footer.php'); ?>