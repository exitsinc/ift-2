<?php include('include/main_header.php'); ?>

<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1>Tolerance in Education Conference <br> February 15-16,2021</h1>
      </div>
    </div>
  </div>
</div>


<section class="conference-banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="conference-BImg">
          <!-- <img src="images/Education English.jpg" alt=""> -->
          <img src="images/edconference.jpg" alt="">
        </div>
      </div>
    </div>
  </div>
</section>

<section class="edu-conference-logo">
  <div class="container">
    <div class="row edu-logo">
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/WhatsApp Image 2018-02-12 at 22.05.59 (2).jpeg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 mb-xs-5">
        <div class="edu-logo-BImg">
          <img src="images/GKE Foundation logo with words.jpg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/rowad 3.png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/شعار نبض الامارات (1).png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/WhatsApp Image 2020-09-18 at 17.10.32 (2).jpeg" alt="">
        </div>
      </div>
    </div>
  </div>
</section>

<section class="conferences-main pt-5">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="jumbotron">
          <!-- <div class="row d-flex justify-content-center">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 d-flex justify-content-center align-items-center">
              <div class="conference-img">
                <img src="images/King.jpg" alt="">
              </div>
            </div>
          </div> -->
          <!-- <h1 class="display-4">Hello, world!</h1> -->
         <div class="row">
           <div class="col-md-8">
             <p class="lead">The International Foundation for Tolerance and The International Organization for Tolerance
are pleased to invite you to take part in this important conference on ‘Tolerance in Education’.
We define tolerance as a value and a process where it focuses on building bridges of
understanding and acceptance of others irrespective of their colour, religion, nationality, social
status, ideology etc. by simply accepting humanity as it is.</p>
           </div>
           <div class="col-md-4">
             <img src="images/edc1.jpg" class="d-block w-100">
           </div>
         </div>
         <div class="row">
          <div class="col-md-4">
            <img src="images/edc2.png" class="d-block w-100">
          </div>
           <div class="col-md-8">
             <p class="lead">Recent years, the world has witnessed a major attack on education and schools by different
political and social groups from all sides. Parents are more concerned of the well being of their
children as well as what they learn and do not learn in schools from KG to university. If we are
concerned about social sustainable development and a safer place for our grandchildren, we
need to focus now on creating a tolerant world. It is our responsibility to guide our children to
see the beauty of the garden instead of hearing the sounds of the guns and motors.</p>
           </div>
           
         </div>
         <div class="row">
          
           <div class="col-md-8">
             <p class="lead">This Conference aims to contribute to this mission</p>
          <p class="lead">The papers and presentations in this Conference will address the many main topics outlined
below. More issues and topics will evolve and will be addressed by our selected panel of
international speakers and researchers from different parts of the world. The conference will
feature concurrent sessions in Arabic and in English so that to give more choices for
participants to select the session they want.</p>
           </div>
           <div class="col-md-4">
            <img src="images/edc3.jpg" class="d-block w-100">
          </div>
         </div>

          
          
          
          <p class="lead">There are no registration fees and the conference is open to everybody. However, for those
who may request a certificate of attendance, there will be a small fee for that.</p>
          <!-- <p class="lead">Dr. King V Cheek <br> Conference Chair</p> -->
          <!-- <hr class="my-4">
          <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
          <p class="lead">
            <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
          </p> -->
        </div>
      </div>
    </div>
  </div>
</section>
<section class="home-register pt-5">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <a href="https://docs.google.com/forms/d/e/1FAIpQLScIJwKotyECtOdhEp0Gk7lblSdSh7KPjfyEf3RF8LNGz9Dagg/viewform?vc=0&c=0&w=1&flr=0">
        <div class="google-reg">
          <img src="images/reg-400x300.png" alt="">
        </div>
        </a>
      </div>
    </div>
  </div>
</section>
<?php include('include/main_footer.php'); ?>