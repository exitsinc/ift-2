<?php include('include/main_header.php') ?>

<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4">Meet Our Speakers</h1>
        <h2 class="mb-4" style="font-size: 3rem;margin-top: -22px;">January 11-12,2021</h2>
      </div>
    </div>
  </div>
</div>
<section class="sepk-main mtb">
  <div class="container">
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <h1>Our Speakers</h1>
        <div class="row speakers">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card speaker-card">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
                  <div class="speaker-img-div">
                    <img class="card-img d-block w-100 speaker-img" src="images/Anil.jpg" alt="Card image cap">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                  <div class="card-body">
                    <h5 class="card-title">Prof. Anil Srivastav, India</h5>
                    <p class="card-text">He is the chairman of the North Eastern India Chamber of Commerce and Industry. He is a visiting professor at MPISSR, Ujjain (M.P.) associated with Indian Council of Social Science Research. He has received numerous awards both in India and overseas for his role in building business and cultural bridges among different nations. He is also a member of the advisory council of the international foundation for tolerance and member of the board of many other regional and international organizations.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row speakers">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card speaker-card">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                  <div class="card-body">
                    <h5 class="card-title">Professor Rajiv Gupta, India </h5>
                    <p class="card-text">Dr. Gupta is a professor at the Institute of Management Studies, Devi Ahilya Vishwavidyalaya,
                      and director, Directorate of Distance Education, Devi Ahilya Vishwavidyalaya, India. He held
                      many positions in the past including the dean of the faculty of management studies at Devi
                      Ahilya Vishwavidyalaya. He has published many articles and papers and spoke at different
                    regional and international conferences. </p>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
                  <div class="speaker-img-div">
                    <img class="card-img d-block w-100 speaker-img" src="images/rajiv.jpeg" alt="Card image cap">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include('include/main_footer.php') ?>