<?php include('include/main_header.php'); ?>
<!-- <section class="topics-ban">
    <div class="top-ban">
        <img src="images/gray-painted-background_53876-94041.jpg" class="d-block w-100" alt="...">
        <h1 style="margin: 0 auto;">Themes & Topics</h1>
    </div>
</section> -->
<!-- <div class="site-blocks-cover overlay" style="background-image: url('images/gray-painted-background_53876-94041.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8
      <h1 class="mb-4">Themes & Topics</h1>
     
    </div>
  </div>
</div> -->


<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4">Themes & Topics</h1>
      </div>
    </div>
  </div>
</div>

<section class="edu-conference-logo mtb">
  <div class="container">
    <div class="row edu-logo">
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/WhatsApp Image 2018-02-12 at 22.05.59 (2).jpeg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 mb-xs-5">
        <div class="edu-logo-BImg">
          <img src="images/GKE Foundation logo with words.jpg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/rowad 3.png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/شعار نبض الامارات (1).png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/WhatsApp Image 2020-09-18 at 17.10.32 (2).jpeg" alt="">
        </div>
      </div>
    </div>
  </div>
</section>

<section class="topic-main mtb">
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
      <h5>The main theme of the conference is Building bridges of understanding and acceptance in education at all levels.  We welcome proposals for research papers and presentations from colleagues on any of the below topics and/or other topics that may be suggested.</h5>
      <!-- <h5>The main theme of the conference is Building bridges of understanding and acceptance
through sports and sports media. We welcome proposals for research papers and
presentations from colleagues on any of the below topics and/or other topics that may be
suggested.</h5> -->
      <ul>
        <li>Building tolerance within students and teachers</li>
        <li>Accepting other viewpoints</li>
        <li>Bullying</li>
        <li>Students’ issues</li>
        <li>Harassment in schools</li>
        <li>Distance education issues</li>
        <li>Tolerance in media</li>
        <li>Social and cultural issues</li>
        <li>Preparing for workplace dynamics</li>
        <li>Best practices</li>
        <li>Others</li>
      </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
      <img src="images/img_4.jpg" class="d-block w-100">
    </div>
  </div>
</div>
</section>
<?php include('include/main_footer.php'); ?>