<?php
ob_start();
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require './PHPMailer/src/Exception.php';
require './PHPMailer/src/PHPMailer.php';
require './PHPMailer/src/SMTP.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    $admin_mail = 'noreply@mx106.in.ddcpl.com';
    $admin_receiver = 'info@iftolerance.org';

    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_SERVER; // Enable verbose debug output
    $mail->isSMTP(); // Send using SMTP
    $mail->Host = 'mx106.in.ddcpl.com'; // Set the SMTP server to send through
    $mail->SMTPAuth = true; // Enable SMTP authentication
    $mail->Username = $admin_mail; // SMTP username
    $mail->Password = 'NoReply@2020'; // SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port = 587; // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom($admin_mail, 'Contact Form Mailer');

    $name = isset($_POST['name']) ? $_POST['name'] : '';
    $subject = isset($_POST['subject']) ? $_POST['subject'] : '';
    $email = isset($_POST['email']) ? $_POST['email'] : '';
    $message = isset($_POST['message']) ? $_POST['message'] : '';

    $mail->addAddress($admin_receiver); // Name is optional
    // var_dump($_POST);
    if (!is_null($email)) {
        $mail->addAddress($email, $name); // Add a recipient
    }
    // $mail->addReplyTo('info@example.com', 'Information');
    // $mail->addCC('cc@example.com');
    // $mail->addBCC('bcc@example.com');

    // Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    // Content
    $mail->isHTML(true); // Set email format to HTML
    $mail->Subject = 'Contact Form from Website :' . $name . " - " . $subject;
    $html = '<table style="width: 100%" border="1" cellpadding="15">';
    $html .= ' <tr>';
    $html .= '    <td>Name:</td>';
    $html .= '    <td>' . $name . '</td>';
    $html .= ' </tr>';
    $html .= '  <tr>';
    $html .= '    <td>subject:</td>';
    $html .= '    <td> ' . $subject . '</td>';
    $html .= ' </tr>';
    $html .= '  <tr>';
    $html .= '    <td>Email:</td>';
    $html .= '    <td>' . $email . '</td>';
    $html .= ' </tr>';
    $html .= '  <tr>';
    $html .= '    <td>Message:</td>';
    $html .= '    <td>' . $message . '</td>';
    $html .= ' </tr>';
    $html .= '</table>';

    $mail->Body = $html;
    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
    header("Location: ". $_SERVER['HTTP_REFERER']."?success=1");

    // header("Location: ./index.php");
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    header("Location: ". $_SERVER['HTTP_REFERER']."?error=1");
}
ob_end_flush();
