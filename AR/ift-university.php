<?php include('include/main_header.php'); ?>
<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4">الجامعة الدولية للتسامح              </h1>
      </div>
    </div>
  </div>
</div>
<!-- <div class="site-blocks-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-8 text-center banner-div" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">الجامعة الدولية للتسامح              </h1>
        <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium facere officiis in quo, corporis quasi.</p>
        <p><a href="#" class="btn btn-primary px-4 py-3">Get Started</a></p>
      </div>
    </div>
  </div>
</div> -->
<section class="eduction-main right-text-class">
  <div class="container">
    <div class="row edu-block">
      <div class="col-md-12">
        <p>الجامعة الدولية للتسامح هي مؤسسة تعليمية مهنية غير اكاديمية وهي ثمرة تعاون بين كل من الهيئة الدولية للتسامح ومقرها الولايات المتحدة الامريكية والمنظمة الدولية للتسامح ومقرها بريطانيا وتهدف الجامعة الى تنمية عملية بناء جسور التفاهم والقبول بين الافراد والمجموعات والثقافات وذلك من خلال توفير فرص وبرامج تعلم وتطوير تتسم بالجانب العملي والتطبيقي . </p>
        <p>تقدم الجامعة الدولية للتسامح جميع برامجها عن طريق التعلم عن بعد وذلك لتوفير التكلفة على المشاركين وفي نفس الوقت توفير الفرص التعليمية للجميع في مختلف اقطار العالم باقل تكلفة ممكنة وتتراوح البرامج بين شهادة او دبلوم او شهادة انجاز برنامج كما تقوم الجامعة بتصميم البرامج المناسبة حسب احتياجات اي مؤسسة وباي لغة . </p>
        <!-- <p>IUOT is the educational arm of these two organizations offering a variety of educational and training programs around the globe. Its programs include certificates and diploma courses. </p>
        <p>The International University of Tolerance offers programs and courses in tolerance, cultural diversity, interfaith communication, education, social behavior, corporate culture, sports etc. We design learning programs based on requests from schools, government and the corporate world. </p> -->
      </div>
      <!-- <div class="col-md-6">
        <img src="images/edu.jpg" class="d-block w-100">
      </div> -->
    </div>
  </div>
</section>
<section class="programs right-text-class">
  <div class="container">
    <div class="row">
      <div class="col-md-6 ">
        <img src="images/PSDL2218.jpg" class="d-block w-100">
      </div>
      <div class="col-md-6 ">
        <div class="program-heading">
          <h1>من البرامج التي ستقدم خلال العام 2021  </h1>
        </div>
        <div class="crt">
          <ul dir="rtl">
            <li>شهادة المعلم المعتمد في التسامح
            </li>
            <li> شهادة المدير المعتمد في التسامح
            </li>
            <li> شهادة المدرب المعتمد في التسامح </li>
            <li> شهادة المدير المعتمد في التسامح </li>
            <li> شهادة التسامح في الرياضة
            </li>
            
          </ul>
        </div>
      </div>
      
    </div>
    
  </div>
  
</section>
<section class="edutext edu-block right-text-class">
  <div class="container edu-con">
    <div class="row ">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <ul dir="rtl">
          <!--  <li>من أجل الوصول إلى أكبر عدد ممكن من المتعلمين ، ستتيح الجامعة الدولية للتسامح جميع برامجها عبر الإنترنت ومن خلال التعلم عن بعد.</li>
          <li>للحصول على معلومات ، يرجى النقر فوق البرنامج لتنزيل التفاصيل الكاملة. </li> -->
          <li>لمزيد من المعلومات يرجى التواصل على الايميل university@iftolerance.org  </li>
        </ul>
      </div>
      
    </div>
  </div>
</section>

<section class="regi-main mtb right-text-class">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="regdata">
          <div class="jumbotron jumbotron-fluid">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                  <a href="images/" download="مسابقة الشباب 1.pdf"><h1 class="display-4" style="    font-size: 3.5rem;">مسابقة البحوث للطلبة والخريجين</h1></a>
                  <!-- <form method="get" action="images/Research competition.pdf">
                    <button type="submit" style="cursor: pointer;"><h1 class="display-4">Research Competition for Students $ Graduates</h1></button>
                  </form> -->
                  
                  <!-- <hr> -->
                </div>
                <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                  <div class="conference-BImg">
                    <a href="images/" download="Research competition.pdf">
                    <img src="images/Education English.jpg" alt="">
                  </a>
                  </div>
                </div> -->
                
                
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
</section>

<?php include('include/main_footer.php'); ?>