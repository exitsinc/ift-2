<?php include('include/main_header.php'); ?>


<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg')">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1>الهيئة الدولية للتسامح </h1>
        <!-- <div class="home-slider">
          <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
              <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
              <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
              <li data-target="#carouselExampleCaptions" data-slide-to="4"></li>
              <li data-target="#carouselExampleCaptions" data-slide-to="5"></li>
              
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active">
                 <img src="./images/george_simons.jpg" class="d-block w-100" alt="..."> 
                
              </div>
              <div class="carousel-item">
                 <img src="./images/joseph 2.jpg" class="d-block w-100" alt="..."> 
                
              </div>
              
              <div class="carousel-item">
                <img src="./images/DianneDetectClose.jpg" class="d-block w-100" alt="..."> 
                
              </div>
              <div class="carousel-item">
                 <img src="./images/Robert Crane 2.png" class="d-block w-100" alt="..."> 
                
              </div>
              <div class="carousel-item">
                 <img src="./images/new-zealand-prime-minister-jacinda-ardern-looks-on-as-she-news-photo-1137537366-1553281560.jpg" class="d-block w-100" alt="..."> 
                
              </div>
              <div class="carousel-item">
                 <img src="./images/DrD90NmXgAAR438.jpg" class="d-block w-100" alt="..."> 
                
              </div>
              
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div> -->
      </div>
      <!--       <div class="col-lg-12 mt-5">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 mb-4">
            <div class="feature-1 d-flex">
              <div class="wpb_wrapper">
                <div class="wpb-img">
                  <img src="./images/ADSITF.png" alt="">
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 mb-4">
            <div class="feature-1 d-flex">
              <div class="form">
                <form>
                  <div class="form-group">
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Your Name">
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="E-Mail">
                    
                  </div>
                  <button type="submit" class="btn btn-primary">Subscribe</button>
                </form>
              </div>
              
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 mb-4">
            <div class="feature-1 d-flex">
              <div class="wpb_wrapper">
                <div class="wpb-img">
                  <a href="https://asian.iftolerance.org/AR/" target="_blank">
                    <img src="./images/THE-ASIAN-REGIONAL-TOLERANCE-ACROSS-CULTURES-CONFERENCE-500x500.png" alt="">
                  </a>
                </div>
                
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 mb-4">
            <div class="feature-1 d-flex">
              <div class="wpb_wrapper">
                <div class="wpb-img">
                  <a href="https://toleranceconference.org/" target="_blank">
                    <img src="./images/Upload.png" alt="">
                  </a>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div> -->
    </div>
  </div>
</div>

<!-- <div class="site-blocks-cover overlay" style="background-image: url('images/Slide1.JPG');">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-8 text-center banner-div" data-aos="fade-up" data-aos-delay="400">
        
      </div>
        <div class="col-md-8 text-center banner-div" >
        
      </div>
    </div>
  </div>
</div> -->
<section class="conference-banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="conference-BImg">
          <!-- <img src="images/homearebicban.jpeg" alt=""> -->
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <table class="paperTable" style="width:100%;text-align:center">
          <tr>
            <th>تنزيل</th>
            <th>العنوان</th>
            <th>رقم</th>
          </tr>
          <tr>
            <td><a href="public/papers/10.pdf" download class="btn btn-primary">تنزيل</a></td>
            <td> كلمة افتتاح سمو الشيخ سالم بن سلطان القاسمي </td>
            <td>1</td>
          </tr>
          <tr>
            <td><a href="public/papers/01.pdf" download class="btn btn-primary">تنزيل</a></td>
            <td>... التسامح في فكر زايد<br>
              الدكتور سميح المجالي
            </td>
            <td>2</td>
          </tr>
          <tr>
            <td><a href="public/papers/02.pdf" download class="btn btn-primary">تنزيل</a></td>
            <td>التسامح والفهم والقبول: مدخل مفاهيمي ومنظور معاصر<br>
              الدكتور جمال مختار خلف</td>
            <td>3</td>
          </tr>
          <tr>
            <td><a href="public/papers/03.pdf" download class="btn btn-primary">تنزيل</a></td>
            <td>التسامح الاجتماعي عنواناً لطلبة كلية الإدارة والاقتصاد في العراق <br>
              الاستاذة الدكتورة ندوة هلال جوده و الدكتورة .وداد ادور وادي</td>
            <td>4</td>
          </tr>
          <tr>
            <td><a href="public/papers/04.pdf" download class="btn btn-primary">تنزيل</a></td>
            <td>الامتنان وعلاقته بالذكاء الذاتي لدى طلبة مدارس المتميزين<br>
              الاستاذة الدكتورة زهرة موسى جعفر والدكتورة سالي علوان
            </td>
            <td>5</td>
          </tr>
          <tr>
            <td><a href="public/papers/05.pdf" download class="btn btn-primary">تنزيل</a></td>
            <td>الحوار لتعزيز التسامح بين اتباع الاديان<br>
              فاطمه احمد ابو سرير</td>
            <td>6</td>
          </tr>
          <tr>
            <td><a href="public/papers/06.pdf" download class="btn btn-primary">تنزيل</a></td>
            <td>دور الرياضة في تعزيز قيم التسامح بين جماهير العالم .<br>
              الدكتور ادريس مغاري </td>
            <td>7</td>
          </tr>
          <tr>
            <td><a href="public/papers/07.pdf" download class="btn btn-primary">تنزيل</a></td>
            <td>الرياضة ودورها في تنمية صفات المحبة والتسامح بين أفراد المجتمع <br>
              الاستاذ سعد ال لجهر </td>
            <td>8</td>
          </tr>
          <tr>
            <td><a href="public/papers/08.pdf" download class="btn btn-primary">تنزيل</a></td>
            <td>التسامح بين المفهوم والامنيات لدى الشباب العربي <br>
              الاستاذ نبيل الحريبي الكثيري </td>
            <td>9</td>
          </tr>
          <tr>
            <td><a href="public/papers/09.pdf" download class="btn btn-primary">تنزيل</a></td>
            <td>خطاب الكراهية في اوساط الشباب : جذور الظاهرة ودور جائحة كورونا في تجاوزها <br>
              الدكتورة كوثر الزغلامي </td>
            <td>10</td>
          </tr>
          <tr>
            <td><a href="public/papers/11.pdf" download class="btn btn-primary">تنزيل</a></td>
            <td>مداخلة الاستاذ عبد المنعم العيد</td>
            <td>11</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</section>

<!-- <section class="conference-banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="conference-BImg">
          <img src="images/Sports Arabic.jpg" alt="">
        </div>
      </div>
    </div>
  </div>
</section> -->

<!-- <section class="conference-banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="conference-BImg">
          <img src="images/Education Arabic.jpg" alt="">
        </div>
      </div>
    </div>
  </div>
</section> -->

<section class="home-register">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <a href="https://docs.google.com/forms/d/e/1FAIpQLSe9lbCiIDTzj5Aorakbj5LIZRUttHtWCI1hLUcPRS5Lux3m7Q/viewform?vc=0&c=0&w=1&flr=0">
          <div class="google-reg">
            <img src="images/reg-400x300.png" alt="">
          </div>
        </a>
      </div>
    </div>
  </div>
</section>
<!-- <section class="zoom-slider1" style="background: url('./images/DSC0155.jpg') no-repeat;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        
      </div>
    </div>
  </div>
</section> -->
<!-- <div class="site-section section-3">
  <div class="container">
    
    <div class="row">
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 home-service d-flex">
          <a href="https://iftolerance.org/EN/contact-us/" target="_self"></a>
          <img src="./images/CULTER.jpg" alt="">
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 home-service d-flex">
          <a href="https://toleranceconference.org/" target="_self"></a>
          <img src="./images/CONF.jpg" alt="">
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 home-service d-flex">
          <a href="https://iftolerance.org/EN/membership/" target="_self"></a>
          <img src="./images/member1.jpg" alt="">
        </div>
      </div>
      
      
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12  col-md-12 col-lg-12 col-xl-12 mb-4">
        <div class="sep">
          <img src="./images/sep.gif" alt="">
        </div>
      </div>
    </div>
    
  </div>
</div> -->
<!-- <div class="site-section section-1">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 mb-md-4">
        <div class="home-video">
          <iframe class="ifram-class" src="https://www.youtube.com/embed/rWYP0uC-FF0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="px-lg-3">
          <p class="dropcap">SPONSOR WIDGET</p>
        </div>
      </div>
      
    </div>
  </div>
</div> -->
<!-- <div class="site-section section-1">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 mb-md-4">
        <img src="images/img_sq.jpg" alt="Image" class="img-fluid">
      </div>
      <div class="col-lg-4">
        <div class="px-lg-3">
          <p class="dropcap">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non temporibus ut quasi aspernatur. Alias rerum tempore animi omnis dolores deleniti quibusdam.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem a possimus quae quis nam ducimus saepe. Facere est maiores unde magnam, incidunt saepe repellendus. Officia natus possimus inventore eius soluta!</p>
        </div>
      </div>
      <div class="col-lg-4">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem a possimus quae quis nam ducimus saepe. Facere est maiores unde magnam, incidunt saepe repellendus. Officia natus possimus inventore eius soluta!</p>
        <p>Facere est maiores unde magnam, incidunt saepe repellendus. Officia natus possimus inventore eius soluta</p>
        
      </div>
    </div>
  </div>
</div> -->
<!-- <div class="section-2">
  <div class="container">
    <div class="row no-gutters align-items-stretch align-items-center">
      <div class="col-lg-3 section-title">
        <div class="service-1-title h-100">
          <h2 class="text-white">Featured Practice Area</h2>
          <p>Lorem ipsum dolor sit consectetur adipisicing elit. Voluptatum dignissimos mollitia vitae accusamus eius doloribus.</p>
          <p><a href="#" class="more">View All</a></p>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="service-1 first h-100" style="background-image: url('images/img_1.jpg')">
          <div class="service-1-contents">
            <span class="wrap-icon">
              <span class="flaticon-balance"></span>
            </span>
            <h2>Business Law</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="service-1 h-100" style="background-image: url('images/img_2.jpg')">
          <div class="service-1-contents">
            <span class="wrap-icon">
              <span class="flaticon-law"></span>
            </span>
            <h2>Criminal Law</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="service-1 h-100" style="background-image: url('images/img_3.jpg')">
          <div class="service-1-contents">
            <span class="wrap-icon">
              <span class="flaticon-courthouse"></span>
            </span>
            <h2>Tax Law</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- <div class="site-section section-3">
  <div class="container">
    <div class="row justify-content-center mb-5">
      <div class="col-lg-6 section-title text-center">
        <h2>Areas of Expertise</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 d-flex">
          <div class="service-2-icon mr-3"><span class="flaticon-law"></span></div>
          <div class="service-2-contents">
            <h3>Insurance Matters</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime harum optio doloribus error dolor.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 d-flex">
          <div class="service-2-icon mr-3"><span class="flaticon-balance"></span></div>
          <div class="service-2-contents">
            <h3>Insurance Matters</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime harum optio doloribus error dolor.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 d-flex">
          <div class="service-2-icon mr-3"><span class="flaticon-auction"></span></div>
          <div class="service-2-contents">
            <h3>Insurance Matters</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime harum optio doloribus error dolor.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 d-flex">
          <div class="service-2-icon mr-3"><span class="flaticon-auction"></span></div>
          <div class="service-2-contents">
            <h3>Insurance Matters</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime harum optio doloribus error dolor.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 d-flex">
          <div class="service-2-icon mr-3"><span class="flaticon-courthouse"></span></div>
          <div class="service-2-contents">
            <h3>Insurance Matters</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime harum optio doloribus error dolor.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="service-2 d-flex">
          <div class="service-2-icon mr-3"><span class="flaticon-law"></span></div>
          <div class="service-2-contents">
            <h3>Insurance Matters</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime harum optio doloribus error dolor.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- <div class="site-section section-4" style="background-image: url('images/hero_2.jpg')">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 mr-auto mb-5">
        <h2 class="mb-3">Why Clients Choose Us?</h2>
        <p class="mb-4 d-block">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet, non.</p>
        <p><a href="#" class="btn btn-primary py-3 px-4">Free Consultation</a></p>
      </div>
      <div class="col-lg-6">
        <div class="row">
          <div class="col-md-6 mb-4">
            <div class="feature-1 d-flex">
              <span class="feature-1-icon mr-3">
                <span class="flaticon-law"></span>
              </span>
              <div class="feature-1-contents">
                <strong class="number">%89</strong>
                <span class="caption">Successful Case</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 mb-4">
            <div class="feature-1 d-flex">
              <span class="feature-1-icon mr-3">
                <span class="flaticon-law"></span>
              </span>
              <div class="feature-1-contents">
                <strong class="number">421</strong>
                <span class="caption">Trusted Clients</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 mb-4">
            <div class="feature-1 d-flex">
              <span class="feature-1-icon mr-3">
                <span class="flaticon-law"></span>
              </span>
              <div class="feature-1-contents">
                <strong class="number">392</strong>
                <span class="caption">Expert Lawyers</span>
              </div>
            </div>
          </div>
          <div class="col-md-6 mb-4">
            <div class="feature-1 d-flex">
              <span class="feature-1-icon mr-3">
                <span class="flaticon-law"></span>
              </span>
              <div class="feature-1-contents">
                <strong class="number">913</strong>
                <span class="caption">Honors and Awards</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- <div class="site-section section-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 mr-auto">
        <h2 class="mb-4">We Are An International Law Group, Provides High <span class="text-primary">Quality Service</span></h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi, accusantium.</p>
        
        <div class="call-now">
          <span>Call Now For Immediate Assistance</span>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-request">
          <h2>Request a free consultation</h2>
          <form action="#">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Name">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Phone">
            </div>
            <div class="form-group">
              <textarea class="form-control" placeholder="Case Description" id="" cols="30" rows="2"></textarea>
            </div>
            <div class="form-group">
              <input type="submit" class="btn btn-primary" value="Send Request">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- <div class="site-section section-6">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 mr-auto">
        <h2 class="mb-5">Happy <span class="text-primary">Clients Says</span></h2>
        <div class="owl-carousel nonloop-block-4 testimonial-slider">
          <div class="testimony-1">
            <div class="d-flex align-items-center mb-4">
              <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
              <div>
                <h3>John Doe</h3>
                <p>Business Man</p>
              </div>
            </div>
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
            </blockquote>
          </div>
          <div class="testimony-1">
            <div class="d-flex align-items-center mb-4">
              <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
              <div>
                <h3>John Doe</h3>
                <p>Business Man</p>
              </div>
            </div>
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
            </blockquote>
          </div>
          <div class="testimony-1">
            <div class="d-flex align-items-center mb-4">
              <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
              <div>
                <h3>John Doe</h3>
                <p>Business Man</p>
              </div>
            </div>
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
            </blockquote>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        
        <h2 class="mb-5">Frequently <span class="text-primary">Ask Questions</span></h2>
        <div class="border p-3 rounded mb-2">
          <a data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="true" aria-controls="collapse-1" class="accordion-item h5 d-block mb-0">Law assistance to my business</a>
          <div class="collapse show" id="collapse-1">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
        <div class="border p-3 rounded mb-2">
          <a data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4" class="accordion-item h5 d-block mb-0">The newest part of my legislation</a>
          <div class="collapse" id="collapse-4">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
        <div class="border p-3 rounded mb-2">
          <a data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-2" class="accordion-item h5 d-block mb-0">Are you an internationla law?</a>
          <div class="collapse" id="collapse-2">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
        <div class="border p-3 rounded mb-2">
          <a data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-3" class="accordion-item h5 d-block mb-0">How the system works?</a>
          <div class="collapse" id="collapse-3">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div> -->
<!-- <div class="bg-primary" data-aos="fade">
  <div class="container">
    <div class="row">
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-facebook text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-twitter text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-instagram text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-linkedin text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-pinterest text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-youtube text-white"></span></a>
    </div>
  </div>
</div> -->
<?php include('include/main_footer.php'); ?>