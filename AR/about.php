<?php include('include/main_header.php'); ?>


<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4">من نحن </h1>
      </div>
    </div>
  </div>
</div>


<!-- <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-8 text-center" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">من نحن </h1>
      </div>
    </div>
  </div>
</div> -->
<!-- mission -->
<div class="site-section section-1 section-1-about bg-light" id="mission-id">
  <div class="container">
    <div class="row">
      <!-- <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 mb-md-4 section-title right-text-class">
        
        
      </div> -->
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-md-4 section-title">
        <div class="px-lg-3">
          <h2 class="text-center">الرسالة  </h2>
          <p class=" right-text-class">رسالتنا هي بناء جسور التفاهم والقبول بين مختلف الافراد والمجموعات والثقافات من خلال التعلم والبحوث والمناهج العلمية بما يحقق  مصلحة الانسانية . </p>
          
        </div>
      </div>
      
    </div>
  </div>
</div>
<!-- vision -->
<div class="site-section section-1 section-1-about bg-light" id="vision">
  <div class="container">
    <div class="row">
      <!-- <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 mb-md-4 section-title right-text-class">
        
        
      </div> -->
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-md-4 section-title">
        <div class="px-lg-3">
          <h2 class="text-center">  الرؤية   </h2>
          <p class=" right-text-class">ان نكون الخيار الاول لتقديم الاستشارات المهنية للتعامل مع الخلافات والازمات الثقافية بين الشعوب او المجموعات ولبناء جسور الاستدامة بين مختلف الثقافات. </p>
          
        </div>
      </div>
      
    </div>
  </div>
</div>

<!-- board of advisiory -->
<div class="site-section section-6 advisory-board-section right-text-class" id="board-of-trustees">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2>مجلس الامناء   </h2>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/HE-Sheikh-Eng.-Salem.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>  الشيخ المهندس سالم بن سلطان القاسمي </h3>
          <!-- <p>رئيس، <br/>
           رأس الخيمة للطيران المدني </p>
          <p> امارات العربية المتحدة </p> -->
          <p>رئيس دائرة الطيران المدني </p>
          <p>راس الخيمة – الامارات العربية المتحدة </p>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/King.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>  الدكتور كينج شيك          </h3>
          <!-- <p>رئيس مجلس الأمناء <br/>
          المؤسسة الدولية للتسامح</p>
          <p>نيويورك ، الولايات المتحدة الأمريكية</p> -->
          <p>رئيس مجلس الامناء </p>
          <p>الهيئة الدولية للتسامح  </p>
          <p>نيويورك – الولايات المتحدة الامريكية  </p>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/joseph 2.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>  الاسقف الدكتور جوزيف جريبوسكي    </h3>
          <!-- <p>سيدي الرئيس ،<br/> المركز الدولي للدين والعدل واشنطن العاصمة ، </p>
          <p>الولايات المتحدة الامريكية </p> -->
          <p>رئيس المركز الدولي للاديان والعدالة </p>
          <p>واشنطن – الولايات المتحدة الامريكية  </p>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/Nicholas Cardy 2.jpeg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>  نيكولاس كاردي          </h3>
          <p>صحفي دولي  </p>
          <p>لندن – المملكة المتحدة  </p>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/Jaames Chang22.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>  جيمس شانك    </h3>
          <!-- <p>رئيس <br/> مؤسسة تبادل المعرفة العالمية </p>
          <p>الولايات المتحدة الامريكية </p> -->
          <p>رئيس الهيئة الدولية لتبادل المعرفة  </p>
          <p>نيويورك – الولايات المتحدة الامريكية  </p>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/WhatsApp Image 2020-06-16 at 14.20.27.jpeg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          
          <h3>  الدكتورة ليلى حبيب البلوشي       </h3>
          <!-- <p>المدير التنفيذي،<br/>
          الفكره للاستشارات الادارية </p>
          <p> الإمارات العربية المتحدة </p> -->
          <p>رئيسة مركز الفكرة للاستشارات الادارية  </p>
          <p>ابوظبي – الامارات العربية المتحدة  </p>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/76.jpeg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3> الدكتور عبدالله صادق دحلان </h3>
          <!-- <p>رئيس<br/>
          جمعية الصحفيين البحرينية </p>
          <p>البحرين </p> -->
          <p> رئيس مجلس الامناء </p>
          <p> جامعة الاعمال والتكنولوجيا </p>
          <p> المملكة العربية السعودية  </p>

        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/Antonio_Imeneo.jpeg" alt="Image" class="vcard mr-4">
            <div>
              
            </div>
          </div>
          <h3>  الدكتور انتونيو ايمونيو  </h3>
          <p>مدير المركز الدولي للبحوث – فونفك اوروبا  </p>
          <p>روما - ايطاليا   </p>

        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/board_2.jpg" alt="Image" class="vcard mr-4">
            <div>
              
            </div>
          </div>
          <h3>محمد علي النقي</h3>
          <p> رئيس مجلس الادارة </p>
          <p> شركة الصناعات الكويتية القابضة </p>
          <p> الكويت </p>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/TaqiAlZeera.jpg" alt="Image" class="vcard mr-4">
            <div>
            </div>
          </div>
          <h3>الدكتور تقي الزيرة</h3>
          <p>رئيس المنتدى الاقتصادي العربي</p>
          <p>مملكة البحرين </p>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- board of advisory -->
<div class="site-section section-6 advisory-board-section right-text-class" id="advisory-board">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2> المجلس الاستشاري      </h2>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/george_simons.jpg" alt="Image" class="vcard mr-4">
            <div>
            </div>
          </div>
          <h3> الدكتور جورج سيمونز   </h3>
          <p>مؤسس ورئيس شركة دايفرسوفي  </p>
          <p>فرنسا  </p>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/Robert Crane 2.png" alt="Image" class="vcard mr-4">
            <div>
            </div>
          </div>
          <h3> الدكتور روبرت كراين    </h3>
          <p>استاذ جامعي وباحث اكاديمي  </p>
          <p>فرنسا  </p>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/DianneDetectClose.jpg" alt="Image" class="vcard mr-4">
            <div>
            </div>
          </div>
          <h3> ديانا سفير هوفنر     </h3>
          <!-- <p>رئيس<br/>
           المخبر الثقافي </p>
          <p>الولايات المتحدة الامريكية </p> -->
          <p>مؤسس ورئيسة شركة  </p>
          <p>الولايات المتحدة الامريكية  </p>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/earl johnson.png" alt="Image" class="vcard mr-4">
            <div>
            </div>
          </div>
          <h3> الدكتور ايرل جونسون     </h3>
          <p>مؤسس ورئيس شركة الاستشارات الدولية  </p>
          <p>الولايات المتحدة الامريكية  </p>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/Daisy-Khan.jpg" alt="Image" class="vcard mr-4">
            <div>
            </div>
          </div>
          <h3> ديزي خان     </h3>
          <p>مؤسس والمدير التنفيذي لمبادرة المراة المسلمة   </p>
          <p>الولايات المتحدة الامريكية  </p>
        </div>
      </div>
       <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/Anil.jpg" alt="Image" class="vcard mr-4">
            <div>
            </div>
          </div>
          <h3> الدكتور انيل سيرفاستاف     </h3>
          <p>رئيس غرفة تجارة وصناعة شمال شرق الهند    </p>
          <p>نيودلهي – الهند  </p>
        </div>
      </div>
      <div class="col-lg-4 mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/Susan leather blazer 2.jpg" alt="Image" class="vcard mr-4">
            <div>
            </div>
          </div>
          <h3>سوزان الن  </h3>
          <p>مؤسس ورئيسة ندوة الزواج  </p>
          <p>الولايات المتحدة الامريكية  </p>
        </div>
      </div>
      <div class="col-lg-4 mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/Indunil 2.jpeg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3> اندونيل فرناندو    </h3>
          <p>الرئيس التنفيذي لمركز العناية الصحية    </p>
          <p>سيرلانكا  </p>
        </div>
      </div>


   
    </div>
  </div>
</div>

<!--     <div class="site-section section-1 section-1-about bg-light">
  <div class="container">
    <div class="row">
      
      <div class="col-lg-6">
        <div class="px-lg-3">
          <h3><b>Mission</b></h3><br/>
          
          <p class="dropcap">Our mission is to build bridges of understanding and acceptance among different people, Groups and cultures through research and scientific approaches for the betterment of humankind.</p>
        </div>
      </div>
      <div class="col-lg-6">
        <h3><b>Vision</b></h3><br/>
        <p class="dropcap">Our vision is To be the primary center of choice for providing professional advisory services on cultural conflict and for building sustainable bridges among diversified cultures.</p>
      </div>
    </div>
  </div>
</div> -->
<!--     <div class="site-section">
  <div class="container">
    <div class="row justify-content-center mb-5">
      <div class="col-lg-6 section-title text-center">
        <h2>BOARD OF TRUSTEES</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia eligendi sint corrupti eos dolor numquam sit error fuga asperiores laborum!</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
        <div class="person-1">
          <img src="images/Rev.-Joseph.jpg" alt="Image" class="img-fluid">
          <div class="person-1-contents">
            <h2>ٌRev. Joseph K. Grieboski, DD, FRSA</h2>
            <span class="d-block position">President<br/>International Center on Religion and Justice<br/>USA</span>
            
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
        <div class="person-1">
          <img src="images/HE-Sheikh-Eng.-Salem.jpg" alt="Image" class="img-fluid">
          <div class="person-1-contents">
            <h2>His Excellency Engineer Sheikh Salem Bin Sultan Al Qasimi</h2>
            <span class="d-block position">Chairman<br/>Ras Al Khaima Civil Aviation<br/>United Arab Emirates</span>
            
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
        <div class="person-1">
          <img src="images/Dr.-King-V.-Cheek.jpg" alt="Image" class="img-fluid">
          <div class="person-1-contents">
            <h2>Dr. King V. Cheek</h2>
            <span class="d-block position">Professor of Social Science<br/>New York Institute of Technology<br/>USA</span>
            
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
        <div class="person-1">
          <img src="images/layla.jpg" alt="Image" class="img-fluid">
          <div class="person-1-contents">
            <h2>Dr. King V. Cheek</h2>
            <span class="d-block position">Professor of Social Science<br/>New York Institute of Technology<br/>USA</span>
            
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
        <div class="person-1">
          <img src="images/Rev.-Joseph.jpg" alt="Image" class="img-fluid">
          <div class="person-1-contents">
            <h2>ٌRev. Joseph K. Grieboski, DD, FRSA</h2>
            <span class="d-block position">President<br/>International Center on Religion and Justice<br/>USA</span>
            
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
        <div class="person-1">
          <img src="images/HE-Sheikh-Eng.-Salem.jpg" alt="Image" class="img-fluid">
          <div class="person-1-contents">
            <h2>His Excellency Engineer Sheikh Salem Bin Sultan Al Qasimi</h2>
            <span class="d-block position">Chairman<br/>Ras Al Khaima Civil Aviation<br/>United Arab Emirates</span>
            
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
        <div class="person-1">
          <img src="images/Dr.-King-V.-Cheek.jpg" alt="Image" class="img-fluid">
          <div class="person-1-contents">
            <h2>Dr. King V. Cheek</h2>
            <span class="d-block position">Professor of Social Science<br/>New York Institute of Technology<br/>USA</span>
            
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
        <div class="person-1">
          <img src="images/layla.jpg" alt="Image" class="img-fluid">
          <div class="person-1-contents">
            <h2>Dr. King V. Cheek</h2>
            <span class="d-block position">Professor of Social Science<br/>New York Institute of Technology<br/>USA</span>
            
          </div>
        </div>
      </div>
    </div>
    <div class="row justify-content-center mb-5">
      <div class="col-lg-6 section-title text-center">
        <h2>ADVISORY BOARD</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia eligendi sint corrupti eos dolor numquam sit error fuga asperiores laborum!</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
        <div class="person-1">
          <img src="images/DR.-GEORGE-F.jpg" alt="Image" class="img-fluid">
          <div class="person-1-contents">
            <h2>ٌDr. George Simons</h2>
            <span class="d-block position">CEO, Diversophy</span>
            
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
        <div class="person-1">
          <img src="images/Dr.-Sandra-Phelps.jpg" alt="Image" class="img-fluid">
          <div class="person-1-contents">
            <h2>Dr. Sandra Phelps</h2>
            <span class="d-block position">CARE international</span>
            
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
        <div class="person-1">
          <img src="images/Dr.-Robert-Crane.jpg" alt="Image" class="img-fluid">
          <div class="person-1-contents">
            <h2>Dr. Robert Crane</h2>
            <span class="d-block position">International scholar</span>
            
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
        <div class="person-1">
          <img src="images/Dianne-Hofner-Saphiere.jpg" alt="Image" class="img-fluid">
          <div class="person-1-contents">
            <h2>Dianne Hofner Saphiere</h2>
            <span class="d-block position">President Cultural Detective</span>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!--     <div class="section-2">
  <div class="container">
    <div class="row no-gutters align-items-stretch align-items-center">
      <div class="col-lg-3 section-title">
        <div class="service-1-title h-100">
          <h2 class="text-white">Featured Practice Area</h2>
          <p>Lorem ipsum dolor sit consectetur adipisicing elit. Voluptatum dignissimos mollitia vitae accusamus eius doloribus.</p>
          <p><a href="#" class="more">View All</a></p>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="service-1 first h-100" style="background-image: url('images/img_1.jpg')">
          <div class="service-1-contents">
            <span class="wrap-icon">
              <span class="flaticon-balance"></span>
            </span>
            <h2>Business Law</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="service-1 h-100" style="background-image: url('images/img_2.jpg')">
          <div class="service-1-contents">
            <span class="wrap-icon">
              <span class="flaticon-law"></span>
            </span>
            <h2>Criminal Law</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="service-1 h-100" style="background-image: url('images/img_3.jpg')">
          <div class="service-1-contents">
            <span class="wrap-icon">
              <span class="flaticon-courthouse"></span>
            </span>
            <h2>Tax Law</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!--     <div class="site-section section-6">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 mr-auto">
        <h2 class="mb-5">Happy <span class="text-primary">Clients Says</span></h2>
        <div class="owl-carousel nonloop-block-4 testimonial-slider">
          <div class="testimony-1">
            <div class="d-flex align-items-center mb-4">
              <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
              <div>
                <h3>John Doe</h3>
                <p>Business Man</p>
              </div>
            </div>
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
            </blockquote>
          </div>
          <div class="testimony-1">
            <div class="d-flex align-items-center mb-4">
              <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
              <div>
                <h3>John Doe</h3>
                <p>Business Man</p>
              </div>
            </div>
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
            </blockquote>
          </div>
          <div class="testimony-1">
            <div class="d-flex align-items-center mb-4">
              <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
              <div>
                <h3>John Doe</h3>
                <p>Business Man</p>
              </div>
            </div>
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
            </blockquote>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        
        <h2 class="mb-5">Frequently <span class="text-primary">Ask Questions</span></h2>
        <div class="border p-3 rounded mb-2">
          <a data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="true" aria-controls="collapse-1" class="accordion-item h5 d-block mb-0">Law assistance to my business</a>
          <div class="collapse show" id="collapse-1">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
        <div class="border p-3 rounded mb-2">
          <a data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4" class="accordion-item h5 d-block mb-0">The newest part of my legislation</a>
          <div class="collapse" id="collapse-4">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
        <div class="border p-3 rounded mb-2">
          <a data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-2" class="accordion-item h5 d-block mb-0">Are you an internationla law?</a>
          <div class="collapse" id="collapse-2">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
        <div class="border p-3 rounded mb-2">
          <a data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-3" class="accordion-item h5 d-block mb-0">How the system works?</a>
          <div class="collapse" id="collapse-3">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>
-->
<div class="bg-primary" data-aos="fade">
  <div class="container">
    <div class="row">
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-facebook text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-twitter text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-instagram text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-linkedin text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-pinterest text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-youtube text-white"></span></a>
    </div>
  </div>
</div>
<?php include('include/main_footer.php'); ?>