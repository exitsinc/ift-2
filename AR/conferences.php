<?php include('include/main_header.php'); ?>

<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4"> المؤتمر الاقليمي الاسيوي للتسامح عبر الثقافات <br>16 – 17 نوفمبر 2020 </h1>
      </div>
    </div>
  </div>
</div>


<section class="conferences-main mt-5  right-text-class">
  <div class="container">

    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="jumbotron">
          <div class="row d-flex justify-content-center">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 d-flex justify-content-center align-items-center">
              <div class="conference-img">
                <img src="images/King.jpg" alt="">
              </div>
            </div>
          </div>
          <!-- <h1 class="display-4">Hello, world!</h1> -->
          <p class="lead">نيابة عن زملائي اعضاء مجلس الامناء والمجلس الاستشاري للهيئة الدولية للتسامح يسرني ان ادعوكم للمشاركة في المؤتمر الاقليمي الاسيوي للتسامح عبر الثقافات الذي سيعقد على مدى يومي 16 و 17 نوفمبر 2020 . ونظرا للظروف غير الطبيعية التي يمر بها العالم نتيجة جائحة كورونا كوفيد 19 فان المؤتمر سيعقد افتراضيا عبر منصة المؤتمرات الافتراضية زوم .</p>
          <p class="lead">يستضيف المؤتمر نخبة ممتازة من الباحثين والمحاضرين من مختلف دول العالم ولذلك فان المحاضرات ستكون باللغتين العربية والانجليزية حيث ستكون هناك حوالي 2 الى 3 جلسات في نفس الوقت بعضها باللغة العربية والبعض باللغة الانجليزية لتتاح لكل مشارك حرية الاختيار بين الجلسات ليس على اساس اللغة فقط ولكن حسب الموضوعات والاهتمامات ، كما سيتم توزيع جميع اوراق العمل والبحوث في كتيب الكتروني .</p>
          <p class="lead">التسجيل في المؤتمر مجاني بدون اية رسوم والتسجيل مفتوح للجميع في اي مكان في العالم ، وبالنسبة للراغبين في الحصول على شهادة حضور المؤتمر فهناك رسوم بسيطة لاصدار الشهادات .
            نتطلع للقاء بكم افتراضيا في يوم التسامح العالمي في افتتاح المؤتمر يوم 16 نوفمبر متمنين لكم الصحة والعناية والوقاية .
          </p>

          <!-- <p class="lead">Dr. King V Cheek <br> Conference Chair</p> -->
          <p class="lead">
            الدكتور كينج شيك
            <br> رئيس مجلس امناء الهيئة الدولية للتسامح </p>
          <!-- <hr class="my-4">
          <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
          <p class="lead">
            <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
          </p> -->
        </div>
      </div>
    </div>
  </div>
</section>
<section class="home-register pt-5">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <h3 style="text-align: center;">المؤتمر االقليمي اآلسيوي للتسامح عبر الثقافات <br>
          2020 نوفمبر 16-17</h3>
        <p style="text-align: center;">برنامج المؤتمر <br>
          بتوقيت مكه المكرمة</p>
        <p style="text-align: right;">2020 / 11 / 16 األثنين</p>
        <table class="confrenceTable">
          <tr>
            <th style="width: 15%;">رئيس الجلسة</th>
            <th style="width: 25%">المحاضرون</th>
            <th>الموضوع / العنوان</th>
            <th style="width: 15%;">التوقيت</th>
            <th>رقم</th>
          </tr>
          <tr>
            <td></td>
            <td>المنسق الفني للقاعة </td>
            <td>فتح قاعة التسجيل والدخول </td>
            <td>4:00 - 3:30</td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td>أمانة المؤتمر</td>
            <td>جراءات تنظيمية </td>
            <td>4:15 - 4:00</td>
            <td></td>
          </tr>
          <tr>
            <td>محمد الزيودي</td>
            <td>سمو الشيخ المهندس سالم بن سلطان القاسمي</td>
            <td>كلمة االفتتاح</td>
            <td>4:20 – 4:15</td>
            <td></td>
          </tr>
          <tr>
            <td>محمد الزيودي</td>
            <td>الدكتور أمين أبو حجله
              <br>الدكتور سميح زيد المجالي
              <br>الدكتور جمال مختار
            </td>
            <td>
              الجلسة األولى / قاعة سلطان العويس
              <br>نماذج وتطبيقات التسامح بين األردن واإلمارات
              <br>التسامح في فكر زايد
              <br>التسامح والفهم والقبول: مدخل مفاهيمي ومنظور معاصر
            </td>
            <td>5:00 - 4:20</td>
            <td>1</td>
          </tr>
          <tr>
            <td>الدكتورة سهير المهندي</td>
            <td>الدكتورة ندوة هالل جوده جوده / الدكتورة وداد ادور وادي
              <br> الدكتورة زهرة موسى والدكتورة سالي علوان
              <br> فاطمة أحمد أبو سرير
            </td>
            <td>
              الجلسة الثانية / قاعة الشيخ عيسى بن راشد
              <br>التسامح االجتماعي عنواناً لطلبة كلية اإلدارة واالقتصاد في العراق
              <br>االمتنان وعالقته بالذكاء الذاتي لدى طلبة مدارس المتميزين
              <br>الحوار لتعزيز التسامح بين اتباع االديان
            </td>
            <td>
              6:00 - 5:05
            </td>
            <td>2</td>
          </tr>
          <tr>
            <td colspan="3">اختتام جلسات اليوم األول وإ لى اللقاء غدا الثالثاء 17 نوفمبر في جلسات اليوم الثاني إن شاء هللا</td>
            <td></td>
            <td></td>
          </tr>
        </table>
        <p class="mt-5 text-right">الثالثاء 17 / 11 / 2020</p>
        <table class="confrenceTable">
          <tr>
          <tr>
            <th style="width: 15%;">رئيس الجلسة</th>
            <th style="width: 25%">المحاضرون</th>
            <th>الموضوع / العنوان</th>
            <th style="width: 15%;">التوقيت</th>
            <th>رقم</th>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td>فتح قاعة التسجيل والدخول + جوانب وتعريفات اجرائية</td>
            <td>4:00 - 3:30</td>
            <td></td>
          </tr>
          <tr>
            <td>دكتور تركي الزهراني</td>
            <td>الدكتور ادريس مغاري
              <br>سعد عبدهللا ال جه
              <br>نبيل الكثيري
            </td>
            <td>
              الجلسة الثالثة: قاعة عبدهللا الدبل
              <br>دور الرياضة في تعزيز قيم التسامح جماهير العالم
              <br>الرياضة ودورها في تنمية صفات المحبة والتسامح بين أفراد المجتمع
              <br>التسامح بين المفهوم واالمنيات لدى الشباب العربي
            </td>
            <td>
              5:00 - 4:00
            </td>
            <td>3</td>
          </tr>
          <tr>
            <td>الدكتورة ليلى البلوشي</td>
            <td>الدكتورة ليلى البلوشي
              <br>الدكتورة كوثر الزغالمي
            </td>
            <td>الجلسة الرابعة قاعة الدكتور عارف العاجل
              <br>التسامح بين أفراد األسرة
              <br>خطاب الكراهية في اوساط الشباب : جذور الظاهرة ودور جائحة كورونا في تجاوزها
            </td>
            <td>6:00 - 5:00</td>
            <td>4</td>
          </tr>
          <tr>
            <td colspan="3">اختتام فعاليات المؤتمر االقليمي االسيوي للتسامح عبر الثقافات</td>
            <td></td>
            <td></td>
          </tr>
        </table>
        <p style="text-align: center;"> البرنامج قابل للتعديل دو ن اشعار مسبق</p>
        <div style="text-align: center;" class="mt-5">
          <a href="public/confrences/conference-program_16-17Nov.pdf" download class="btn btn-primary">Download Schedule</a>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <a href="https://docs.google.com/forms/d/e/1FAIpQLSe9lbCiIDTzj5Aorakbj5LIZRUttHtWCI1hLUcPRS5Lux3m7Q/viewform?vc=0&c=0&w=1&flr=0">
          <div class="google-reg">
            <img src="images/reg-400x300.png" alt="">
          </div>
        </a>
      </div>
    </div>
  </div>
</section>
<?php include('include/main_footer.php'); ?>