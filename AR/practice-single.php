<?php include('include/main_header.php'); ?> 
    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/hero_3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-8 text-center" data-aos="fade-up" data-aos-delay="400">
            <span class="caption text-uppercase text-white">Practice Area</span>
            <h1 class="mb-4">Business Law</h1>
          </div>
        </div>
      </div>
    </div>
    

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <img src="images/img_1.jpg" alt="Image" class="img-fluid">
          </div>
          <div class="col-lg-6 pl-lg-5">
            <h2 class="mb-4">Business Law</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt debitis magni nobis ad blanditiis, tempore rem quia error hic quasi corporis similique perferendis, dolorum optio amet distinctio quam esse ipsam!</p>
            <p>Eum illum fugit magni voluptatem, veniam hic ratione impedit atque. Soluta laborum incidunt aut. Expedita libero ex, possimus inventore qui doloremque cumque. Qui saepe voluptate, quos molestiae minima in delectus?</p>
            <p>Dolores recusandae hic fugiat quam, amet sapiente voluptatibus saepe nihil deserunt! Repellat sapiente laboriosam eligendi eaque odit dignissimos reprehenderit corrupti ex saepe cum, tempora quas, ut ad nisi, amet repudiandae!</p>
          </div>
        </div>
      </div>
    </div>
    
    
    <div class="site-section pb-0">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-lg-6 section-title text-center">
            <h2>More Practive Area</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint dolor soluta quo quaerat saepe cupiditate ipsum earum est ipsa quos.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="section-2">
      <div class="container">
        
        <div class="row no-gutters align-items-stretch align-items-center">
          <div class="col-lg-4">
            <div class="service-1 first h-100" style="background-image: url('images/img_1.jpg')">
              <div class="service-1-contents">
                <span class="wrap-icon">
                  <span class="flaticon-balance"></span>
                </span>
                <h2>Business Law</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="service-1 h-100" style="background-image: url('images/img_2.jpg')">
              <div class="service-1-contents">
                <span class="wrap-icon">
                  <span class="flaticon-law"></span>
                </span>
                <h2>Criminal Law</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="service-1 h-100" style="background-image: url('images/img_3.jpg')">
              <div class="service-1-contents">
                <span class="wrap-icon">
                  <span class="flaticon-courthouse"></span>
                </span>
                <h2>Tax Law</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="service-1 first h-100" style="background-image: url('images/img_1.jpg')">
              <div class="service-1-contents">
                <span class="wrap-icon">
                  <span class="flaticon-balance"></span>
                </span>
                <h2>Business Law</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="service-1 h-100" style="background-image: url('images/img_2.jpg')">
              <div class="service-1-contents">
                <span class="wrap-icon">
                  <span class="flaticon-law"></span>
                </span>
                <h2>Criminal Law</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="service-1 h-100" style="background-image: url('images/img_3.jpg')">
              <div class="service-1-contents">
                <span class="wrap-icon">
                  <span class="flaticon-courthouse"></span>
                </span>
                <h2>Tax Law</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem vel odit tempora itaque, repellendus distinctio harum totam. Rerum.</p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    
    

    


   

    <div class="site-section section-6">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 mr-auto">
            <h2 class="mb-5">Happy <span class="text-primary">Clients Says</span></h2>
            <div class="owl-carousel nonloop-block-4 testimonial-slider">
              <div class="testimony-1">
                <div class="d-flex align-items-center mb-4">
                  <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
                  <div>
                    <h3>John Doe</h3>
                    <p>Business Man</p>
                  </div>
                </div>
                <blockquote>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
                </blockquote>
              </div>

              <div class="testimony-1">
                <div class="d-flex align-items-center mb-4">
                  <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
                  <div>
                    <h3>John Doe</h3>
                    <p>Business Man</p>
                  </div>
                </div>
                <blockquote>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
                </blockquote>
              </div>


              <div class="testimony-1">
                <div class="d-flex align-items-center mb-4">
                  <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
                  <div>
                    <h3>John Doe</h3>
                    <p>Business Man</p>
                  </div>
                </div>
                <blockquote>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
                </blockquote>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            
            <h2 class="mb-5">Frequently <span class="text-primary">Ask Questions</span></h2>

            <div class="border p-3 rounded mb-2">
              <a data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="true" aria-controls="collapse-1" class="accordion-item h5 d-block mb-0">Law assistance to my business</a>

              <div class="collapse show" id="collapse-1">
                <div class="pt-2">
                  <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
                </div>
              </div>
            </div>

            <div class="border p-3 rounded mb-2">
              <a data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4" class="accordion-item h5 d-block mb-0">The newest part of my legislation</a>

              <div class="collapse" id="collapse-4">
                <div class="pt-2">
                  <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
                </div>
              </div>
            </div>

            <div class="border p-3 rounded mb-2">
              <a data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-2" class="accordion-item h5 d-block mb-0">Are you an internationla law?</a>

              <div class="collapse" id="collapse-2">
                <div class="pt-2">
                  <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
                </div>
              </div>
            </div>

            <div class="border p-3 rounded mb-2">
              <a data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-3" class="accordion-item h5 d-block mb-0">How the system works?</a>

              <div class="collapse" id="collapse-3">
                <div class="pt-2">
                  <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>





    

    <div class="bg-primary" data-aos="fade">
      <div class="container">
        <div class="row">
          <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-facebook text-white"></span></a>
          <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-twitter text-white"></span></a>
          <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-instagram text-white"></span></a>
          <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-linkedin text-white"></span></a>
          <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-pinterest text-white"></span></a>
          <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-youtube text-white"></span></a>
        </div>
      </div>
    </div>

<?php include('include/main_footer.php'); ?>
