<?php include('include/main_header.php'); ?>

<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
       <h1 class="mb-4">مؤتمرات اخرى </h1>
      </div>
    </div>
  </div>
</div>


<section class="regi-main mtb right-text-class">
	<div class="container">
		<div class="regdata">
			<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">مؤتمرات اخرى </h1>
    <hr>
    <ul dir="rtl">
     <!--  <li> مؤتمر التسامح في الرياضة  يناير 2021  </li>
      <li> المؤتمر الاقليمي الافريقي للتسامح عبر الثقافات مارس 2021</li>
      <li> المؤتمر الاقليمي الاوروبي للتسامح عبر الثقافات ابريل 2021 </li>
      <li> المؤتمر الاقليمي الامريكي للتسامح عبر الثقافات مايو 2021 </li> -->
       <li>المؤتمر الاقليمي الافريقي للتسامح عبر الثقافات مارس 2021</li>
      <li>المؤتمر الاقليمي الاوروبي للتسامح عبر الثقافات ابريل 2021</li>
      <li>المؤتمر الاقليمي الامريكي للتسامح عبر الثقافات مايو 2021</li>
    </ul>
   <!--  <p class="lead">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</a></p> -->
  </div>
</div>
		</div>
	</div>
</section>
<?php include('include/main_footer.php'); ?>