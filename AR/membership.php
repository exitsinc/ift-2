<?php include('include/main_header.php'); ?>


<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4 membership-h2">العضوية</h1>
      </div>
    </div>
  </div>
</div>




<section class="benifits right-text-class">
	<div class="container">
		<div class="jumbotron">
			<h2 class="display-4 membership-display-4">ساهم في فعاليات الهيئة الدولية للتسامح من خلال الانضمام لعضوية الهيئة وتعبئة طلب العضوية 
</h2>
			<h2 class="lead mt-4 membership-p"> مزايا العضوية  </h2>
			<hr class="my-4">
			<!-- <p>By being a member of the International Foundation for Tolerance you will</p> -->
			<ul dir="rtl">
				<li>   الحصول على  شهادة عضوية الهيئة الدولية للتسامح وهي عضوية سنوية   </li>
				<li>  استلام رقم عضوية يمكن استخدامه في كل المراسلات والفعاليات 
</li>
				<li> استلام دعوات لجميع المؤتمرات والفعاليات التي تنظمها الهيئة 
 </li>
				<li> الحصول على النشرة الدورية  </li>
				<li> الحصول على خصومات في رسوم المؤتمرات والفعاليات  </li>
				<li>       استلام دعوات للمشاركة في المشروعات المختلفة التي تطرحها الهيئة 				</li>
				<li>    يمكنك التطوع والعمل ضمن لجان ومشروعات الهيئة 
				</li>
			</ul>
			<h2 class="lead mt-4 membership-p">رسوم العضوية </h2>
			<hr class="my-4">
			<p>ررسوم العضوية السنوية هي 25 دولار يمكن دفعها عن طريق بطاقة الائتمان بعد تعبئة طلب العضوية </p>
			<hr class="my-4">
			<div class="crt">
						<img src="images/crt 102.jpg " class="d-block w-100">
			</div>
			
		</div>
	</div>
</section>
<section class="membership-form  right-text-class">
	<div class="container">
		<div class="mem-form">
			<h2>طلب العضوية </h2>
			<hr>
			<form>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputName">الاسم الكامل </label>
						<input type="name" class="form-control" id="inputName">
					</div>
					<div class="form-group col-md-6">
						<label for="inputEmail4">الايميل
						</label>
						<input type="email" class="form-control" id="inputEmail4">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputDob">تاريخ الميلاد</label>
						<input type="dob" class="form-control" id="inputDob">
					</div>
					<div class="form-group col-md-6">
						<label for="inputCity">المدينة والدولة مقر الاقامة حاليا </label>
						<input type="city" class="form-control" id="inputCity">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputDob">رقم الهاتف / الجوال
						</label>
						<input type="dob" class="form-control" id="inputDob">
					</div>
					<div class="form-group col-md-6">
						<label for="inputCity">رقم الواتس اب اذا كان مختلفا
						</label>
						<input type="city" class="form-control" id="inputCity">
					</div>
				</div>
				<!-- <div class="form-row">
						<div class="form-group">
								<label>Are you a student ? </label>
								<div class="form-check form-check-inline">
										<input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
										<label class="form-check-label" for="inlineCheckbox1">Yes</label>
								</div>
								<div class="form-check form-check-inline">
										<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
										<label class="form-check-label" for="inlineCheckbox2">No</label>
								</div>
						</div></div>
						<div class="form-group">
								<label for="inputAddress2">If yes write name of school/college</label>
								<input type="text" class="form-control" id="inputAddress2" placeholder="College or School Name">
						</div>
						<div class="form-row">
								<div class="form-group">
										<label>Are you employed ?  </label>
										<div class="form-check form-check-inline">
												<input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
												<label class="form-check-label" for="inlineCheckbox1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
												<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
												<label class="form-check-label" for="inlineCheckbox2">No</label>
										</div>
								</div></div>
								<div class="form-group">
										<label for="inputAddress2">If yes write Name of employer</label>
										<input type="text" class="form-control" id="inputAddress2" placeholder="Employer Name">
								</div>
								<div class="form-row">
										<div class="form-group">
												<label>Are you self-employed/freelancer ? </label>
												<div class="form-check form-check-inline">
														<input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
														<label class="form-check-label" for="inlineCheckbox1">Yes</label>
												</div>
												<div class="form-check form-check-inline">
														<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
														<label class="form-check-label" for="inlineCheckbox2">No</label>
												</div>
										</div></div>
										<div class="form-group">
												<label for="inputAddress2">If yes write Company Name and Website</label>
												<input type="text" class="form-control" id="inputAddress2" placeholder="Company or Website Name">
							</div> -->
							<div class="form-group ">
								<label for="inputCity">اية ملاحظات اخرى
								</label>
								<textarea type="text" class="form-control" id="inputCity"></textarea>
							</div>
							<!-- <div class="form-group ">
									<button type="submit" class="btn btn-primary">Sign in</button>
							</div> -->
							<!-- <div class="form-row form-group">
									<div class="form-group col-md-3">
											<label for="inputEmail4">Security check code :</label>
											
									</div>
									<div class="form-group col-md-3">
									<label for="inputEmail4"><?php echo uniqid(); ?></label>
									
								</div>
								<div class="form-group col-md-6">
									<label for="inputPassword4">Code</label>
									<input type="Amount" class="form-control" id="inputAmount" placeholder="Enter Security check code">
								</div>
							</div> -->
							<div class="form-group row">
								 <div class="col-sm-6">
										<button type="Cancel" class="btn btn-primary">Cancel</button>
								</div>
								<div class="col-sm-6">
										<!-- <div id="paypal-button-container"></div> -->
										<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">

<input type="hidden" name="cmd" value="_s-xclick">

<input type="hidden" name="hosted_button_id" value="QNMALURH5G3LL">

<table>

<tr><td><input type="hidden" name="on0" value="Certificates شهادات">شهادات</td></tr><tr><td><select name="os0">

                <option value="المؤتمر الاسيوي للتسامح">المؤتمر الاسيوي للتسامح $25.00 USD</option>

                <option value="مؤتمر التسامح في الرياضة">مؤتمر التسامح في الرياضة $30.00 USD</option>

                <option value="مؤتمر التسامح في التعليم">مؤتمر التسامح في التعليم $30.00 USD</option>

                <!-- <option value="Asian Tolerance">Asian Tolerance $25.00 USD</option>

                <option value="Tolerance in Sports">Tolerance in Sports $30.00 USD</option>

                <option value="Tolerance in Education">Tolerance in Education $30.00 USD</option>
 -->
</select> </td></tr>

</table>

<input type="hidden" name="currency_code" value="USD">

<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">

<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">

</form>
								</div> 
								
								
							</div>
							<div class="jumbotron mt-5 text-danger">
								<p>ببتعبئة هذا الطلب وتقديمه فانك تقر بانك قد قرأت رسالة ورؤية الهيئة الدولية للتسامح وانك ستعمل على بناء جسور التفاهم والقبول بين الجميع من اجل مصلحة الانسانية . 
								</p>
								<button type="button" class="btn btn-primary">اضغط هنا للموافقة والدفع</button>
							</div>
						</form>
					</div>
				</section>
				<?php include('include/main_footer.php'); ?>