<?php include('include/main_header.php'); ?>

<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
         <h1 class="mb-4">تواصل معنا</h1>
      </div>
    </div>
  </div>
</div>


<div class="site-section right-text-class" style="padding-bottom: 0;">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <p class="lead">للتواصل</p>
        <p class="lead"> يمكن التواصل من خلال تعبئة النموذج او ارسال ايميل مباشرة </p>
        
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 col-xl-7 mb-5">
        <form action="../contact_process.php" method="POST" class="contact-form">
          <div class="row form-group">
            <div class="col-md-12 mb-3 mb-md-0">
              <label class="font-weight-bold" for="name">الاسم الكامل</label>
              <input type="text" id="name" class="form-control" >
               </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label class="font-weight-bold" for="email">البريد الإلكتروني</label>
              <input type="email" id="email" class="form-control" >
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label class="font-weight-bold" for="subject">موضوع</label>
              <input type="text" id="subject" class="form-control" >
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label class="font-weight-bold" for="message">رسالة</label>
              <textarea name="message" id="message" cols="30" rows="5" class="form-control"
              ></textarea>
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <input type="submit" value="أرسل رسالة" class="btn btn-primary py-3 px-4">
            </div>
          </div>
        </form>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 ml-auto">
        <div class="p-4 mb-3 bg-white cont">
          <h3 class="h5 text-black mb-3">الهيئة الدولية للتسامح</h3>
          <p class="mb-0 font-weight-bold text-black">International Foundation for Tolerance <br>Professional Corporation</p>
          <p class="mb-4 text-black">3911 Concord Pike #8030 SMB #6370 <br>Wilmington, DE 19803, USA</p>
          <hr>
          <h3 class="h5 text-black mb-3">European Office <br></h3>
          <p class="mb-0 font-weight-bold text-black">International Foundation for Tolerance</p>

          <p class="mb-4 text-black">L. BEETHOVEN n ° 2 <br>Anguillara Sabazia <br>Rome 00061, Italy</p>
         
          <p class="mb-0 font-weight-bold text-black">Email Address</p>
          <p class="mb-0"><a href="#">info@iftolerance.org</a></p>
        </div>
      </div>
    </div>
  </div>
</div>


<section class="home-register pt-5 ">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center" style="border: 2px solid;
    border-radius: 15px;
    margin: 0 auto;
    ">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
       <a href="https://docs.google.com/forms/d/e/1FAIpQLSe9lbCiIDTzj5Aorakbj5LIZRUttHtWCI1hLUcPRS5Lux3m7Q/viewform?vc=0&c=0&w=1&flr=0">
        <div class="google-reg">
          <img src="images/reg-400x300.png" alt="">
        </div>
      </a>
    </div>
    <div class="col-md-6">
      
      <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">

        <input type="hidden" name="cmd" value="_s-xclick">

        <input type="hidden" name="hosted_button_id" value="QNMALURH5G3LL">

        <table>

          <tr><td style="font-size: 24px;font-weight:600;"><input type="hidden" name="on0" value="شهادات"> شهادات</td></tr><tr><td><select name="os0">

                <option value="المؤتمر الاسيوي للتسامح">المؤتمر الاسيوي للتسامح $25.00 USD</option>

                <option value="مؤتمر التسامح في الرياضة">مؤتمر التسامح في الرياضة $30.00 USD</option>

                <option value="مؤتمر التسامح في التعليم">مؤتمر التسامح في التعليم $30.00 USD</option>

          </select> </td></tr>

        </table>

        <input type="hidden" name="currency_code" value="USD" >

        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" style="margin-top:10px;height: 30px;">

        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</table>
      </form>
    </div>
  </div>
</div>
</section>
<?php include('include/main_footer.php'); ?>