<?php include('include/main_header.php'); ?>
<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
	<div class="container">
		<div class="row align-items-center justify-content-center">
			<div class="col-md-8 text-center" data-aos="fade-up" data-aos-delay="400">
				<h1 class="mb-4">مجلس الامناء   </h1>
			</div>
		</div>
	</div>
</div>
<div class="site-section section-6 advisory-board-section right-text-class">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2>مجلس الامناء   </h2>
			</div>
			<div class="col-lg-4 mr-auto  mt-5">
				<div class="testimony-1">
					<div class="d-flex align-items-center justify-content-end mb-4">
						<img src="images/HE-Sheikh-Eng.-Salem.jpg" alt="Image" class="vcard mr-4">
						<div>
							
							<!-- <p>Business Man</p> -->
						</div>
					</div>
					<h3>	الشيخ المهندس سالم بن سلطان القاسمي </h3>
					<!-- <p>رئيس، <br/>
					 رأس الخيمة للطيران المدني </p>
					<p> امارات العربية المتحدة </p> -->
					<p>رئيس دائرة الطيران المدني </p>
					<p>راس الخيمة – الامارات العربية المتحدة </p>
				</div>
			</div>
			<div class="col-lg-4 mr-auto  mt-5">
				<div class="testimony-1">
					<div class="d-flex align-items-center justify-content-end mb-4">
						<img src="images/King.jpg" alt="Image" class="vcard mr-4">
						<div>
							
							<!-- <p>Business Man</p> -->
						</div>
					</div>
					<h3>	الدكتور كينج شيك          </h3>
					<!-- <p>رئيس مجلس الأمناء <br/>
					المؤسسة الدولية للتسامح</p>
					<p>نيويورك ، الولايات المتحدة الأمريكية</p> -->
					<p>رئيس مجلس الامناء </p>
					<p>الهيئة الدولية للتسامح  </p>
					<p>نيويورك – الولايات المتحدة الامريكية  </p>
				</div>
			</div>
			<div class="col-lg-4 mr-auto  mt-5">
				<div class="testimony-1">
					<div class="d-flex align-items-center justify-content-end mb-4">
						<img src="images/joseph 2.jpg" alt="Image" class="vcard mr-4">
						<div>
							
							<!-- <p>Business Man</p> -->
						</div>
					</div>
					<h3>	الاسقف الدكتور جوزيف جريبوسكي    </h3>
					<!-- <p>سيدي الرئيس ،<br/> المركز الدولي للدين والعدل واشنطن العاصمة ، </p>
					<p>الولايات المتحدة الامريكية </p> -->
					<p>رئيس المركز الدولي للاديان والعدالة </p>
					<p>واشنطن – الولايات المتحدة الامريكية  </p>
				</div>
			</div>
			<div class="col-lg-4 mr-auto  mt-5">
				<div class="testimony-1">
					<div class="d-flex align-items-center justify-content-end mb-4">
						<img src="images/Nicholas Cardy 2.jpeg" alt="Image" class="vcard mr-4">
						<div>
							
							<!-- <p>Business Man</p> -->
						</div>
					</div>
					<h3>	نيكولاس كاردي          </h3>
					<p>صحفي دولي  </p>
					<p>لندن – المملكة المتحدة  </p>
				</div>
			</div>
			<div class="col-lg-4 mr-auto  mt-5">
				<div class="testimony-1">
					<div class="d-flex align-items-center justify-content-end mb-4">
						<img src="images/Jaames Chang22.jpg" alt="Image" class="vcard mr-4">
						<div>
							
							<!-- <p>Business Man</p> -->
						</div>
					</div>
					<h3>	جيمس شانك    </h3>
					<!-- <p>رئيس <br/> مؤسسة تبادل المعرفة العالمية </p>
					<p>الولايات المتحدة الامريكية </p> -->
					<p>رئيس الهيئة الدولية لتبادل المعرفة  </p>
					<p>نيويورك – الولايات المتحدة الامريكية  </p>
				</div>
			</div>
			<div class="col-lg-4 mr-auto  mt-5">
				<div class="testimony-1">
					<div class="d-flex align-items-center justify-content-end mb-4">
						<img src="images/WhatsApp Image 2020-06-16 at 14.20.27.jpeg" alt="Image" class="vcard mr-4">
						<div>
							
							<!-- <p>Business Man</p> -->
						</div>
					</div>
					
					<h3>	الدكتورة ليلى حبيب البلوشي       </h3>
					<!-- <p>المدير التنفيذي،<br/>
					الفكره للاستشارات الادارية </p>
					<p> الإمارات العربية المتحدة </p> -->
					<p>رئيسة مركز الفكرة للاستشارات الادارية  </p>
					<p>ابوظبي – الامارات العربية المتحدة  </p>
				</div>
			</div>
			<div class="col-lg-4 mr-auto  mt-5">
				<div class="testimony-1">
					<div class="d-flex align-items-center justify-content-end mb-4">
						<img src="images/Ahdiya 2.jpeg" alt="Image" class="vcard mr-4">
						<div>
							
							<!-- <p>Business Man</p> -->
						</div>
					</div>
					<h3>	عهدية احمد السيد  </h3>
					<!-- <p>رئيس<br/>
					جمعية الصحفيين البحرينية </p>
					<p>البحرين </p> -->
					<p>رئيسة جميعة الصحفيين البحرينيين  </p>
					<p>مملكة البحرين   </p>

				</div>
			</div>
			
		</div>
	</div>
</div>
<!-- <div class="site-section section-1 section-1-about bg-light">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 mb-md-4 text-center">
				<h1>THANK YOU!</h1>
				<h3>Lorem ipsum dolor, sit amet consectetur</h3>
				<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta sequi quo vel ipsum laboriosam aliquam laborum officia atque fugiat. Sunt unde expedita labore ea dolorum officia reprehenderit repellendus quos laboriosam.</p>
			</div>
			
			<div class="col-lg-4 mb-md-4 section-title">
				<h2>Lorem </h2>
				<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </p>
			</div>
			<div class="col-lg-8">
				<div class="px-lg-3">
					<p class="dropcap">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nisi, laudantium? Similique mollitia ut, nisi voluptas expedita ex corrupti eaque excepturi nesciunt deserunt harum aliquam, tempore velit error facilis quam voluptates.</p>
					
					<ul>
						<li><a href="javascript:void();">Lorem ipsum dolor</a></li>
						<li><a href="javascript:void();">Lorem ipsum dolor</a></li>
						<li><a href="javascript:void();">Lorem ipsum dolor</a></li>
						<li><a href="javascript:void();">Lorem ipsum dolor</a></li>
						<li><a href="javascript:void();">Lorem ipsum dolor</a></li>
						<li><a href="javascript:void();">Lorem ipsum dolor</a></li>
						
					</ul>
				</div>
			</div>
			
		</div>
	</div>
</div> -->
<?php include('include/main_footer.php'); ?>