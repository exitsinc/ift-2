<?php include('include/main_header.php'); ?>

<div class="site-blocks-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-8 text-center banner-div" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">Training</h1>
        <!-- <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium facere officiis in quo, corporis quasi.</p>
        <p><a href="#" class="btn btn-primary px-4 py-3">Get Started</a></p> -->
      </div>
    </div>
  </div>
</div>
<section class="eduction-main">
	<div class="container">
		<div class="row edu-block">
			<div class="col-md-12">
				<p>Lorem ipsum dolor sit amet consectetur adipisicing, elit. Itaque repellendus, non veniam illo eveniet modi magni cumque placeat cum suscipit voluptates officia consequatur, delectus doloribus enim recusandae dolorem reprehenderit voluptas.</p>
				
				<p>Lorem ipsum dolor sit amet consectetur adipisicing, elit. Itaque repellendus, non veniam illo eveniet modi magni cumque placeat cum suscipit voluptates officia consequatur, delectus doloribus enim recusandae dolorem reprehenderit voluptas.</p>
				
				<p>Lorem ipsum dolor sit amet consectetur adipisicing, elit. Itaque repellendus, non veniam illo eveniet modi magni cumque placeat cum suscipit voluptates officia consequatur, delectus doloribus enim recusandae dolorem reprehenderit voluptas.</p>
				
			</div>	
				
		</div>
	</div>
</section>

<?php include('include/main_footer.php'); ?>