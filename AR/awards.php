<?php include('include/main_header.php'); ?>

<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4">الجوائز والتكريم </h1>
      </div>
    </div>
  </div>
</div>



<!-- <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
	<div class="container">
		<div class="row align-items-center justify-content-center">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center" data-aos="fade-up" data-aos-delay="400">
				<h1 class="mb-4">الجوائز والتكريم </h1>
			</div>
		</div>
	</div>
</div> -->
<div class="site-section section-1 section-1-about bg-light right-text-class">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-md-4 section-title">
				<h2 class="text-center">الجوائز والتكريم</h2>
				<p>تقوم الهيئة الدولية للتسامح بتكريم الافراد والمؤسسات التي ساهمت في بناء جسور التفاهم والقبول بين الافراد والمجموعات والثقافات ويكون التكريم من خلال تقديم اوسمة او شهادات تقديرية . ويمكن ان تكون هذه المساهمات في شكل مساهمات مهنية في مجال البحوث او التعليم او الاعمال التطوعية او المساهمات المالية والعينية لدعم فعاليات وبرامج الهيئة . </p>
				<p>ستعلن الهيئة عن هذه الاوسمة والجوائز سنويا وتفتح باب الترشيح . </p>
			</div>
			<!-- <div class="col-lg-8">
					<div class="px-lg-3">
							<p class="dropcap">The Goals of IFT are…</p><br/>
							<ul>
									<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
									<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
									<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
									<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
									<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
							</ul>
					</div>
			</div> -->
			
		</div>
	</div>
</div>
<section class="eduction-main right-text-class">
	<div class="container">
		<div class="row edu-block">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 pb-2">
				<!-- <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Officiis, tempora! Earum, incidunt maxime cumque facere? Possimus nihil fugiat consequuntur itaque obcaecati impedit amet non quo, eligendi. Tempore, repudiandae quod aperiam?</p>
				<p>Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Officia explicabo molestias nobis ratione saepe commodi debitis maxime quibusdam totam omnis quae reprehenderit, harum possimus, repellendus numquam, voluptate alias odio corporis.</p> -->
				<img src="images/PSDL2218.JPG" class="d-block w-100">
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
				<img src="images/PSDL2227.JPG" class="d-block w-100">
			</div>
		</div>
	</div>
</section>
<?php include('include/main_footer.php'); ?>