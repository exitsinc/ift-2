<footer class="site-footer">
  <div class="container">
    <div class="row">
      <!--  <div class="col-lg-4">
        <div class="mb-5">
          <h3 class="footer-heading mb-4">About Deejee</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa? Ut veritatis, quos illum totam quis blanditiis, minima minus odio!</p>
        </div>
        <div class="mb-5">
          <h3 class="footer-heading mb-4">Subscribe</h3>
          <form action="#" method="post" class="site-block-subscribe">
            <div class="input-group mb-3">
              <input type="text" class="form-control border-secondary bg-transparent" placeholder="Enter your email"
              aria-label="Enter Email" aria-describedby="button-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary rounded-top-right-0" type="button" id="button-addon2">Subscribe</button>
              </div>
            </div>
          </form>
        </div>
      </div> -->
      <!--   <div class="col-lg-4 mb-5 mb-lg-0">
        <div class="row mb-5">
          <div class="col-md-12">
            <h3 class="footer-heading mb-4">Navigations</h3>
          </div>
          <div class="col-md-6 col-lg-6">
            <ul class="list-unstyled">
              <li><a href="#">Home</a></li>
              <li><a href="#">Practive Area</a></li>
              <li><a href="#">Case Studies</a></li>
              <li><a href="#">Careers</a></li>
            </ul>
          </div>
          <div class="col-md-6 col-lg-6">
            <ul class="list-unstyled">
              <li><a href="#">About Us</a></li>
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">Contact Us</a></li>
              <li><a href="#">Membership</a></li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <h3 class="footer-heading mb-4">Follow Us</h3>
            <div>
              <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
              <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
              <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
              <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
            </div>
          </div>
        </div>
      </div> -->
      <!-- <div class="col-lg-4 mb-5 mb-lg-0">
        <div class="mb-5">
          <h3 class="footer-heading mb-4">Watch Live Streaming</h3>
          <div class="block-16">
            <figure>
              <img src="images/img_sq.jpg" alt="Image placeholder" class="img-fluid rounded">
              <a href="https://vimeo.com/channels/staffpicks/93951774" class="play-button popup-vimeo"><span class="icon-play"></span></a>
            </figure>
          </div>
        </div>
        
      </div> -->
      
    </div>
    <div class="row text-center">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
    <p style="color: #000;margin:0">
        <strong>
        Strategic Partner
        </strong></p>
        <img src="images/LOGO_UNIFUNVIC_footer.jpg" alt="">
      </div>
      <div class="col-md-12">
        <p>
          <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          Copyright &copy;<script>document.write(new Date().getFullYear());</script> International Foundation for Tolerance
          <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        </p>
      </div>
      
    </div>
  </div>
  <div class="scroll-up-class smooth-scroll-class" style="padding:10px;position: fixed;
    right: 20px;bottom: 20px;">
    <a href="#navbar-id">
      <img src="images/small-up-arrows-in-a-circle-icon.png" style="width: 50px;" alt="">
    </a>
  </div>
</footer>
</div>
<script src="https://www.paypal.com/sdk/js?client-id=sb&currency=USD" data-sdk-integration-source="button-factory"></script>
<script>
paypal.Buttons({
style: {
shape: 'rect',
color: 'gold',
layout: 'vertical',
label: 'pay',

},
createOrder: function(data, actions) {
return actions.order.create({
purchase_units: [{
amount: {
value: '25'
}
}]
});
},
onApprove: function(data, actions) {
return actions.order.capture().then(function(details) {
alert('Transaction completed by ' + details.payer.name.given_name + '!');
});
}
}).render('#paypal-button-container');
</script>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/mediaelement-and-player.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script src="js/aos.js"></script>
<!-- <script src="js/circleaudioplayer.js"></script> -->
<script src="js/main.js"></script>
<script src="js/custom.js"></script>

</body>
</html>