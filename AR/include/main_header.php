<!DOCTYPE html>
<html lang="en">
  <head>
    
    <title>International Foundation Tolerance – International Foundation Tolerance</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900|Oswald:400,700">
    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/mediaelementplayer.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="css/fl-bigmug-line.css">
    
    
    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/about.css">
    <link rel="stylesheet" href="css/membership.css">
    <link rel="stylesheet" href="css/education.css">
    <link rel="stylesheet" href="css/conferences.css">
    <link rel="stylesheet" href="css/donate.css">
    <link rel="stylesheet" href="css/contact.css">
    <link rel="stylesheet" href="css/speakers.css">
    <link rel="stylesheet" href="css/themestopics.css">
  </head>
  <body>
    
    
    
    <div class="site-wrap">
      <div class="site-navbar" id="navbar-id">
        <div class="container py-1">
          
          <div class="row align-items-center">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div class="logo">
                <a href="index.php" class="text-white h2 mb-0">
                  <img src="./images/banner.jpg" alt="logo">
                </a>
              </div>
            </div>
            
            <div class="col-4 col-md-4 col-lg-12 col-xl-12 ml-auto">
              <nav class="navbar navbar-expand-lg navbar-dark test-nav">
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav">
                <span class="navbar-toggler-icon"><img src="images/2292434-200.png" class="mobile-view-icon" alt=""></span>
                </button>
                <div class="collapse navbar-collapse test-menu" id="main_nav">
                  <!-- <ul class="navbar-nav">
                    
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">  Treeview menu  </a>
                      <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#"> Dropdown item 1 </a></li>
                        <li><a class="dropdown-item" href="#"> Dropdown item 2 &raquo </a>
                        <ul class="submenu dropdown-menu">
                          <li><a class="dropdown-item" href="">Submenu item 1</a></li>
                          <li><a class="dropdown-item" href="">Submenu item 2</a></li>
                          <li><a class="dropdown-item" href="">Submenu item 3 &raquo </a>
                          <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item" href="">Multi level 1</a></li>
                            <li><a class="dropdown-item" href="">Multi level 2</a></li>
                          </ul>
                        </li>
                        <li><a class="dropdown-item" href="">Submenu item 4</a></li>
                        <li><a class="dropdown-item" href="">Submenu item 5</a></li>
                      </ul>
                    </li>
                    <li><a class="dropdown-item" href="#"> Dropdown item 3 </a></li>
                    <li><a class="dropdown-item" href="#"> Dropdown item 4 </a>
                  </ul>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">  More items  </a>
                  <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#"> Dropdown item 1 </a></li>
                    <li><a class="dropdown-item dropdown-toggle" href="#"> Dropdown item 2 </a>
                    <ul class="submenu dropdown-menu">
                      <li><a class="dropdown-item" href="">Submenu item 1</a></li>
                      <li><a class="dropdown-item" href="">Submenu item 2</a></li>
                      <li><a class="dropdown-item" href="">Submenu item 3</a></li>
                    </ul>
                  </li>
                  <li><a class="dropdown-item dropdown-toggle" href="#"> Dropdown item 3 </a>
                  <ul class="submenu dropdown-menu">
                    <li><a class="dropdown-item" href="">Another submenu 1</a></li>
                    <li><a class="dropdown-item" href="">Another submenu 2</a></li>
                    <li><a class="dropdown-item" href="">Another submenu 3</a></li>
                    <li><a class="dropdown-item" href="">Another submenu 4</a></li>
                  </ul>
                </li>
                <li><a class="dropdown-item dropdown-toggle" href="#"> Dropdown item 4 </a>
                <ul class="submenu dropdown-menu">
                  <li><a class="dropdown-item" href="">Another submenu 1</a></li>
                  <li><a class="dropdown-item" href="">Another submenu 2</a></li>
                  <li><a class="dropdown-item" href="">Another submenu 3</a></li>
                  <li><a class="dropdown-item" href="">Another submenu 4</a></li>
                </ul>
              </li>
              <li><a class="dropdown-item" href="#"> Dropdown item 4 </a></li>
              <li><a class="dropdown-item" href="#"> Dropdown item 5 </a></li>
            </ul>
          </li>
        </ul> -->
        <?php  $request_uri= $_SERVER['REQUEST_URI'];?>
        <ul class="navbar-nav ml-auto">
          
          
          
          
          <li class="nav-item"><a class="nav-link" href="contact.php">  تواصل معنا </a></li>
          <li class="nav-item"><a class="nav-link" href="aregistration.php">التسجيل  </a></li>
          <li class="nav-item"><a class="nav-link" href="awards.php">  الجوائز والتكريم </a></li>
          <li class="nav-item"><a class="nav-link" href="donate.php">  التبرعات  </a></li>
          <li class="nav-item dropdown">
            <a class="nav-link  dropdown-toggle" href="javascript:void();" data-toggle="dropdown">  المؤتمرات </a>
            <ul class="dropdown-menu dropdown-menu-right">
              
              <li>
                <a class="dropdown-item dropdown-toggle" href="javascript:void();">المؤتمر الاقليمي الاسيوي للتسامح عبر الثقافات   </a>
                <ul class="submenu submenu-left dropdown-menu">
                  <li><a class="dropdown-item" href="conferences.php">كلمة رئيس الهيئة   </a></li>
                  <li><a class="dropdown-item" href="themetopics.php">المحاور والموضوعات     </a></li>
                  <li><a class="dropdown-item" href="registration.php">التسجيل  </a></li>
                  <li><a class="dropdown-item" href="speaker.php">المحاضرون  </a></li>
                  <li><a class="dropdown-item" href="conference-contact.php">التواصل  </a></li>
                </ul>
              </li>
              <!-- <li>
                <a class="dropdown-item dropdown-toggle" href="javascript:void();"> مؤتمر التسامح في الرياضة والاعلام الرياضي
                </a>
                <ul class="submenu submenu-left dropdown-menu">
                  <li><a class="dropdown-item" href="sconferences.php">المؤتمر  </a></li>
                  <li><a class="dropdown-item" href="sthemetopics.php">المحاور والموضوعات  </a></li>
                  <li><a class="dropdown-item" href="sregistration.php">التسجيل   </a></li>
                  <li><a class="dropdown-item" href="sspeaker.php">المحاضرون </a></li>
                </ul>
              </li> -->
              <!-- <li>
                <a class="dropdown-item dropdown-toggle" href="javascript:void();"> مؤتمر التسامح في التربية والتعليم
                </a>
                <ul class="submenu submenu-left dropdown-menu">
                  <li><a class="dropdown-item" href="edconferences.php"> المؤتمر           </a></li>
                  <li><a class="dropdown-item" href="edthemetopics.php"> المحاور والموضوعات       </a></li>
                  <li><a class="dropdown-item" href="edregistration.php">التسجيل     التواصل   </a></li>
                  <li><a class="dropdown-item" href="edspeaker.php">المحاضرون </a></li>
                </ul>
              </li> -->
              <li><a class="dropdown-item" href="other-conferences.php">مؤتمرات اخرى  </a></li>
            </ul>
          </li>
          <li class="nav-item"><a class="nav-link" href="ift-university.php">  الجامعة  </a></li>
          <li class="nav-item"><a class="nav-link" href="membership.php">  العضوية  </a></li>
          <li class="nav-item dropdown">
            <a href="about.php" class="btn btn-default nav-link dropdown-toggle"  id="navbarDropdown"  role="button" aria-haspopup="true" aria-expanded="false">  من نحن  </a>
            
            <div class="dropdown-menu dropdown-level-1" aria-labelledby="navbarDropdown" >
              <a class="dropdown-item smooth-scroll-class" href="<?php if((strpos($request_uri,"about")!==false)){echo "#mission-id";}else{echo "about.php";} ?>"> الرسالة </a>
              <a class="dropdown-item smooth-scroll-class" href="<?php if((strpos($request_uri,"about")!==false)){echo "#vision";}else{echo "about.php";} ?>"> الرؤية </a>
              
              <a class="dropdown-item smooth-scroll-class" href="<?php if((strpos($request_uri,"about")!==false)){echo "#board-of-trustees";}else{echo "about.php";} ?>"> مجلس الامناء </a>
              <a class="dropdown-item smooth-scroll-class" href="<?php if((strpos($request_uri,"about")!==false)){echo "#advisory-board";}else{echo "about.php";} ?>"> المجلس الاستشاري </a>
              
            </div>
          </li>
          <li class="nav-item active"> <a class="nav-link" href="index.php"> الرئيسية </a> </li>
          <li class="nav-item lang-change-class"><a href="../EN/">ENGLISH</a></li>
        </ul>
        </div> <!-- navbar-collapse.// -->
      </nav>
    </div>
  </div>
</div>
</div>
</div>
<div class="site-mobile-menu">
<div class="site-mobile-menu-header">
<div class="site-mobile-menu-close mt-3">
  <span class="icon-close2 js-menu-toggle"></span>
</div>
</div>
<div class="site-mobile-menu-body"></div>
</div> <!-- .site-mobile-menu -->