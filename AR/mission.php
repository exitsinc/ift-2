<?php include('include/main_header.php'); ?>
<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-8 text-center" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">الرسالة   </h1>
      </div>
    </div>
  </div>
</div>
<div class="site-section section-1 section-1-about bg-light">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 mb-md-4 section-title right-text-class">
        <h2>الرسالة  </h2>
        
      </div>
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 right-text-class">
        <div class="px-lg-3">
          <p>رسالتنا هي بناء جسور التفاهم والقبول بين مختلف الافراد والمجموعات والثقافات من خلال التعلم والبحوث والمناهج العلمية بما يحقق  مصلحة الانسانية . </p>
          
        </div>
      </div>
      
    </div>
  </div>
</div>
<?php include('include/main_footer.php'); ?>