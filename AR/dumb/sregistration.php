<?php include('include/main_header.php'); ?>
<!-- <section class="reg-ban">
	<div class="regi-ban">
		<img src="https://ctusms.com/wp-content/uploads/2019/01/registration-banner-1200x400.jpg" class="d-block w-100" alt="...">
	</div>
</section>
 -->

<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
         <h1 class="mb-4">المشاركة والتسجيل </h1>
      </div>
    </div>
  </div>
</div>

<section class="sport-conference-logo mtb">
  <div class="container">
    <div class="row sport-logo">
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/logo europe.jpg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 mb-xs-5">
        <div class="sport-logo-BImg">
          <img src="images/WhatsApp Image 2020-09-13 at 21.23.00 (1).jpeg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/rowad 3.png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/logo YUOI.png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/WhatsApp Image 2020-09-18 at 17.10.32 (2).jpeg" alt="">
        </div>
      </div>
    </div>
  </div>
</section>

<section class="regi-main mtb  right-text-class">
	<div class="container">
		<div class="regdata">
			<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">المشاركة والتسجيل    </h1>
    <hr>
    <p class="lead"> الدعوة مفتوحة للجميع للمشاركة في المؤتمر حيث ان المشاركة مجانية بدون رسوم تسجيل ، الا ان التسجيل المسبق ضروري ويمكن ان يتم ذلك من خلال تعبئة طلب التسجيل على الموقع مباشرة او التواصل مع امانة المؤتمر بالايميل   ، وسيتم ارسال رسالة تاكيد التسجيل مع رقم التسجيل للمرجعية وسترسل قبل المؤتمر تعليمات الرابط وكيفية التواصل </p>
    <p class="lead"> بالنسبة للمشاركين الذين يرغبون الحصول على شهادات حضور المؤتمر فهناك رسوم رمزية لاصدار الشهادات وهي 30 ثلاثون  دولار امريكي . 
</p>
    <p class="lead"> اذا كنت ترغب في الحصول على شهادة الحضور للمؤتمر نرجو الضغط هنا لدفع رسوم اصدار الشهادة 
 </p>
    <p class="lead"> ونرجو التواصل مع امانة المؤتمر لاية استفسارات <a href="#"> info@iftolerance.org</a></p>
  </div>
</div>
		</div>
	</div>
</section>

<section class="regi-main mtb right-text-class">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="regdata">
          <div class="jumbotron jumbotron-fluid">
            <div class="container">
             
              <h1 class="display-4">  للتواصل   </h1>
              <hr>
              <p class="lead"> امانة المؤتمر </p>
              <p class="lead"> <a href="info@iftolerance.org">info@iftolerance.org</a> </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="home-register pt-5">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center" style="border: 2px solid;
    border-radius: 15px;
    margin: 0 auto;
    ">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
      <a href="https://docs.google.com/forms/d/e/1FAIpQLSfCqDNUnNfbyZCwiXtAFFrXkPgtH2rhW7QCgCEYv62fVnt6WA/viewform">
        <div class="google-reg">
          <img src="images/reg-400x300.png" alt="">
        </div>
      </a>
    </div>
    <div class="col-md-6">
      
      <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">

        <input type="hidden" name="cmd" value="_s-xclick">

        <input type="hidden" name="hosted_button_id" value="QNMALURH5G3LL">

        <table>

          <tr><td style="font-size: 24px;font-weight:600;"><input type="hidden" name="on0" value="شهادات"> شهادات</td></tr><tr><td><select name="os0">

                <option value="المؤتمر الاسيوي للتسامح">المؤتمر الاسيوي للتسامح $25.00 USD</option>

                <option value="مؤتمر التسامح في الرياضة">مؤتمر التسامح في الرياضة $30.00 USD</option>

                <option value="مؤتمر التسامح في التعليم">مؤتمر التسامح في التعليم $30.00 USD</option>

          </select> </td></tr>

        </table>

        <input type="hidden" name="currency_code" value="USD" >

        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" style="margin-top:10px;height: 30px;">

        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">

      </form>
    </div>
  </div>
</div>
</section>

<!-- <section class="reg-register pt-5">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <div id="paypal-button-container"></div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <a href="https://docs.google.com/forms/d/e/1FAIpQLSfCqDNUnNfbyZCwiXtAFFrXkPgtH2rhW7QCgCEYv62fVnt6WA/viewform">
          <div class="google-reg">
            <img src="images/reg-400x300.png" alt="">
          </div>
        </a>
      </div>


    </div>
  </div>
</section> -->
<?php include('include/main_footer.php'); ?>