<?php include('include/main_header.php'); ?>
<!-- <section class="topics-ban">
	<div class="top-ban">
		<img src="images/gray-painted-background_53876-94041.jpg" class="d-block w-100" alt="...">
		<h1 style="margin: 0 auto;">Themes & Topics</h1>
	</div>
</section> -->


<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4"> المحاور والموضوعات
  </h1>
      </div>
    </div>
  </div>
</div>

<section class="edu-conference-logo mtb">
  <div class="container">
    <div class="row edu-logo">
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/WhatsApp Image 2018-02-12 at 22.05.59 (2).jpeg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 mb-xs-5">
        <div class="edu-logo-BImg">
          <img src="images/GKE Foundation logo with words.jpg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/rowad 3.png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/شعار نبض الامارات (1).png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/WhatsApp Image 2020-09-18 at 17.10.32 (2).jpeg" alt="">
        </div>
      </div>
    </div>
  </div>
</section>

<section class="topic-main mtb text-right">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
				<h5> الهدف العام للمؤتمر هو تسليط الضوء كيفية تعزيز التسامح الايجابي من خلال خلق جسور التفاهم والقبول والاحترام ضمن البيئة التعليمية وخارجها ولذلك فان المحاور التالية تمثل بعض الامثلة للمناقشات والبحوث المقدمة 
 </h5>
        <h5>وسيناقش المؤتمر من خلال البحوث واوراق العمل المقدمة عدة موضوعات منها </h5>
           <ul dir="rtl">
           	<li>بناء جسور التسامح في الاطار المدرسي   </li>
           	<li> قبول وجهات النظر الاخرى  </li>
           	<li>التعلم عن بعد والتسامح 
 </li>
           	<li>التحرش في المدارس والجامعات 
 </li>
           	<li>التنوع الثقافي والشرائح المجتمعية  </li>
           	<li>التنمر والعنف 
</li>
           	<li> الاعداد لمرحلة ما بعد التعليم  </li>
           	<li> افضل الممارسات 
 </li>
           	<li> موضوعات اخرى  </li>
           <!-- <li>  افضل الممارسات عالميا </li>
           <li>  موضوعات اخرى</li> -->
           </ul>
			</div>				
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
				<img src="images/img_4.jpg" class="d-block w-100">
			</div>				
		</div>
	</div>
</section>
<?php include('include/main_footer.php'); ?>