<?php include('include/main_header.php') ?>
<!-- <section class="sep-ban">
  <div class="sepk-ban">
    <img src="https://wp.the-eea.com/wp-content/uploads/2019/07/Meet-the-Speakers-banner-1-2000x750.jpg" class="d-block w-100" alt="...">
  </div>
</section> -->
<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4">المحاضرون  </h1>
      </div>
    </div>
  </div>
</div>
<section class="sepk-main mtb  right-text-class">
  <div class="container">
    <!--  <div class="row">
      <div class="col-md-4">
        <img class="d-block w-100" src="images/person_2.jpg" alt="">
      </div>
      <div class="col-md-6">
        <h5>Professor King Cheek</h5>
        <p class="">Dr. King Virgil Cheek, J.D. is a lifelong educator who has served as President of Shaw
          University from 1969 to 1971, and also served as the 8th President of Morgan State
          University from 1971 to 1974.  He is the author of numerous books including The Quadrasoul,
          novels that explore the four dimensions of the human spirit. He is member of the Board of
        Trustees of the International Foundation for Tolerance.</p>
      </div>
    </div> -->
    <div class="row speakers right-text-class">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <h1 class="right-text-class">المحاضرون  </h1>
        <!-- <hr> -->
        
        <div class="row speakers">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card speaker-card">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                  <div class="card-body">
                    <h5 class="card-title">  الدكتور إكرامي بسيوني عبد الحي خطاب – جمهورية مصر العربية   </h5>
                    <p class="card-text">     الدكتور اكرامي بسيوني هو -أستاذ مشارك القانون الإداري والدستوري في قسم القانون بكلية العلوم والدراسات الإنسانية، في جامعة شقراء بالمملكة العربية السعودية وهو حاصل على شهادة الدكتوراه في القانون منذ عام 2010 وله عدة بحوث ومؤلفات منشورة كما انه عضو في الهيئات العلمية والاستشارية لعدد من المجلات المتخصصة مثل المجلة المصرية للدراسات القانونية والاقتصادية، ومجلة القانون الدستوري والعلوم الإدارية الصادرة عن المركز الديمقراطي العربي، برلين، المانيا ومجلة دراسات في العلوم الإنسانية والاجتماعية في الاردن.      </p>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
                  <div class="speaker-img-div">
                    
                    <img class="card-img d-block w-100 speaker-img" src="images/اكرامي بسيوني.png" alt="Card image cap">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                
                <img class="card-img d-block w-100 speaker-img" src="images/7.png" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">الدكتور ملفي الرشيدي  - المملكة العربية السعودية</h5>
                <p class="card-text">الدكتور ملفي  هو رئيس قسم الاساليب الكمية في كلية ادارة الاعمال في جامعة الملك فيصل وقد شغل سابقا منصب عميد كلية المجتمع وهو حاصل على شهادة الدكتورة في بحوث العمليات من بريطانيا وماجستير الاحصاء التطبيقي ، وله مجموعة من المقالات واوراق العمل بالاضافة الى انه ترجم  ثلاث كتب متخصصة من اللغة الانجليزية الى العربية
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row speakers  right-text-class">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card speaker-card">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                  <div class="card-body">
                    <h5 class="card-title"> الدكتورة سهير سند المهندي – مملكة البحرين </h5>
                    <p class="card-text">
                      الدكتورة سهير المهندي من الامثلة الأكاديمية العلمية المتميزة التي لها بصمات بحثية واهتمام بالبحث والباحثين والعلماء  لها مسيرة متميزة في المجال الأعلامي والرياضي  لتخصصها بكالريوس تربية رياضية وماجستير في الإدارة في المجال الرياضي تخصص اعلام رياضي وماجستير في العلاقات العامة من الأكاديمية الأمريكية  كما خاضت عدة مجالات علمية تخصصية اخرى و منها بكالريوس قانون وماجتسير في العلوم الجنائية كما حصلت على الدكتوراه في مجال علم النفس   فهي اكاديمية وصحفية ومذيعة كمقدمة ومعدة برامج  اضافة الى عملها الى عملها كعضو هيئة تدريس غير متفرغ في عدد من الجامعات الخاصة بمملكة البحرين وفي نفس الوقت اعلامية متعاونه مع بضع المؤسسات الاعلامية الصحفية والاذاعية والتلفزيونية في المملكة وغيرها من منطقة دول الخليج العربي .
                      
                    </p>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
                  <div class="speaker-img-div">
                    
                    <img class="card-img d-block w-100 speaker-img" src="images/lasta.jpg" alt="Card image cap">
                  </div>
                </div>
                
                
              </div>
            </div>
          </div>
        </div>

                <div class="row speakers  right-text-class">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card speaker-card">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
                  <div class="speaker-img-div">
                    
                    <img class="card-img d-block w-100 speaker-img" src="images/WhatsApp Image 2020-09-26 at 08.11.19.jpeg" alt="Card image cap">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                  <div class="card-body">
                    <h5 class="card-title">الدكتور عبداللطيف بن إبراهيم بخاري – المملكة العربية السعودية </h5>
                    <p class="card-text">
                      الدكتور عبداللطيف استاذ الادارة والقانون الرياضي في كلية التربية البدنية في جامعة ام القرى في المملكة العربية السعودية وقد حصل على دكتوراه  الادارة الرياضية من جامعة بتسبرج في الولايات المتحدة الامريكية وماجستير ادارة الانشطة الرياضية من جامعة توليدو في امريكا ايضا وله عدة بحوث منشوررة ومشاركات في مؤتمرات مهنية محلية واقليمية وعالمية .
                    </p>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">ناصر محمد  - البحرين</h5>
                <p class="card-text">ناصر محمد أحد الأسماء المعروفة في الوسط الرياضي في مملكة البحرين، فهو صحفي له عمود ثابت في الصفحات الرياضية، وهو مؤرخ رياضي. حيث تفرغ لتدوين تاريخ الرياضة في البحرين والخليج، وله عدة إصدارات لتوثيق ذلك. كما أنه قام بإعداد مجموعة من الحلقات على شبكة يوتيوب يتحدث في كل حلقة عن قصة صورة من الصور التاريخية. وقد تولى رئاسة القسم الرياضي بجريدة "الأيام". ومراسل جريدة "الأنباء" الكويتية منذ بداية تأسيسها، كما عمل مراسل لعدة صحف عربية. </p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                
                <img class="card-img d-block w-100 speaker-img" src="images/16.jpg" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                
                <img class="card-img d-block w-100 speaker-img" src="images/دكتور نبيل طه 2.jpeg" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">الدكتور نبيل طه  - البحرين </h5>               
                <p class="card-text">حاصل على دكتوراه في علم النفس الايجابي من جامعة اوتاوا في كندا بالاضافة الى دراسات عليا في التربية البدنية وعلم النفس الرياضي في جامعة كاليفورنيا ويشغل حاليا منصب مدير إدارة التدريب والتطوير الرياضي باللجنة الأولمبية البحرينية ، كما انه محاضر دولي في كرة اليد من سنة 1996 ومحاضر في البرنامج الوطني لإعداد المدربين في المجال الرياضي مؤهلاً من قبل الجمعية الوطنية للمدربين بكندا منذ عام 1998. كما انه  المدير الفني لمنتخب الشبباب الكويتي لكرة اليد منذ عام 2006 </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--<div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">الدكتور محمد موسى بابا عمي  – الجزائر   </h5>
                <p class="card-text">من ابرز الكتاب في المغرب العربي فهو مؤسس والمشرف العام على مبادرة وسام العالم الجزائري ومدير معهد المناهج للدراسات العليا كما نه من مؤسسي والمشرف العام على مشاريع مكتب الدراسات العلمية، والمدارس العلمية الجديدة وعضو المجمع العلمي؛ لمعهد المناهج، ومسؤول عن مجلة المجمع العلمي وهو حاصل على درجة الدكتوراة في العقيدة ومقارنة الأديان في موضوع  "أصول البرمجة الزمنية في الفكر الإسلامي" ولديه اكثر من 35 كتابا ومؤلفا بالاضافة الى المقالات المختلفة .</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                
                <img class="card-img d-block w-100 speaker-img" src="images/4.jpg" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                
                <img class="card-img d-block w-100 speaker-img" src="images/5.jpeg" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">الدكتورة ليلى حبيب البلوشي  – الامارات العربية المتحدة   </h5>
                <p class="card-text">الدكتورة ليلى هي المدير العام لمركز الفكرة للاستشارات الاداية في الامارات وهي عضو مجلس امناء الهيئة الدولية للتسامح ولديها اكثر من 25 عاما من الخبرة في القطاع المصرفي وفي مجالات التدريب وتنمية الموارد البشرية حيث قدمت استشارات لعدد من المؤسسات والافراد وقد اصدرت كتابا موجها لخريجي الثانوية العامة وشاركت في تاليف كتاب اخر في التناغم الثقافي ، كما انها نفذت عدد من الدورات التدريبية في الولايات المتحدة الامريكية والمانيا وفرنسا والنمسا وفنلندا والصين والهند لكبريات الشركات العالمية .
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">الدكتور إدريس مغاري  – المملكة المغربية</h5>
                <p class="card-text">الدكتور ادريس هو مؤسس أكاديمية فكر للبحث والاستشارات الرياضية وهو استاذ ومدرب بوزارة التربية والتعليم العالي وعضو رئيسي بمجلس جائزة دراسا لعلوم الرياضة بدبي بالإمارات العربية المتحدة كما انه عمل كمدرب رئيسي لدورة تدريبية في مجال القيم الرياضية لفائدة الإداريين والمدربين الرياضيين التابعين للأندية الرياضية المنضوية تحت لواء مجلس الشارقة الرياضي بحكومة الشارقة  ، وهو حاصل على درجة الدكتورة في فلسفة الرياضة ، وله عدة بحوث ومقالات في مجال التخصص.</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                
                <img class="card-img d-block w-100 speaker-img" src="images/6.jpg" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <!--     <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                
                <img class="card-img d-block w-100 speaker-img" src="images/7.png" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">الدكتور ملفي الرشيدي  - المملكة العربية السعودية </h5>
                <p class="card-text">الدكتور ملفي  هو رئيس قسم الاساليب الكمية في كلية ادارة الاعمال في جامعة الملك فيصل وقد شغل سابقا منصب عميد كلية المجتمع وهو حاصل على شهادة الدكتورة في بحوث العمليات من بريطانيا وماجستير الاحصاء التطبيقي ، وله مجموعة من المقالات واوراق العمل بالاضافة الى انه ترجم  ثلاث كتب متخصصة من اللغة الانجليزية الى العربية .   </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <!--  <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                
                <img class="card-img d-block w-100 speaker-img" src="images/8.jpg" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">الاستاذة كوثر الزغلامي   - تونس</h5>
                <p class="card-text">تعمل الاستاذة كوثر كمديرة مؤسسة تربوية ومنشطة برتبة استاد أول فوق الرتبة شباب و طفولة وهي تعد الجزء الاخير من بحث الدكتوراه ولها عدة مساهمات مجتمعية في تونس والامارات العربية المتحدة كما شاركت في تقديم اوراق عمل في عدد من الملتقيات والمؤتمرات المهنية محليا وعالميا .</p>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title"> الاستاذ نبيل   الحريبي الكثيري  - الامارات   </h5>
                <p class="card-text">الكاتب والاعلامي نبيل الحريبي الكثيري احد ابرز الفعاليات الشبابية في الامارات فهو كاتب واعلامي وله عدة روايات ومجموعة من الكتب وقد تميز باحتضانه لمجموعات من الشباب من مختلف الجنسيات العربية وتشجيعهم على الابداع والكتابة والنشر وتنظيم العديد من الفعاليات باستخدام وسائل التواصل الاجتماعي وسيقدم في المؤتمر تقريرا عن بحث ميداني عن الشباب ومفهوم التسامح .</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                
                <img class="card-img d-block w-100 speaker-img" src="images/9.jpg" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                
                <img class="card-img d-block w-100 speaker-img" src="images/10.jpeg" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">الدكتور سميح المجالي     المملكة الاردنية الهاشمية </h5>
                <p class="card-text">الدكتور سميح المجالي لديه دكتوراه في العلوم الاجتماعية تخصص علم الجريمه وهو محاضر في مركز الجامعة العربية للدراسات الامنية والمصرفية للتدريب في الاردن وهو عضو في جميعة الطب الشرعي والادالة الجنائية في جامعة نايف الامنية بالمملكة العربية السعودية وله عدة بحوث في مجالات العنف ضد المراة ودور مواقع التواصل الاجتماعي في ارتفاع السلوك الاجرامي .</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">الدكتور أمين عبدالهادي ابو حجله      المملكة الاردنية الهاشمية </h5>
                <p class="card-text">الدكتور امين ابو حجله هو رئيس فرسان السلام في المملكة الاردنية الهاشمية ويحمل درجة الدكتوراه في الدراسات الاستراتيجية
                  ويشغل عدة مناصب منها مستشار الدراسات الامنية والاستراتيجية ومدير مكتب الاردن لسفراء العالم للسلام كما انه ممثل لعدد كبير
                من الهيئات الدولية في مجال السلام وحقوق الانسان </p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                
                <img class="card-img d-block w-100 speaker-img" src="images/11.jpeg" alt="Card image cap">
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                
                <img class="card-img d-block w-100 speaker-img" src="images/12.jpeg" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">الدكتور جمال عبد الله مخلف المختار     العراق  </h5>
                <p class="card-text">الدكتور جمال المختار رئيس قسم تقنيات إدارة المواد في المعهد التقني بالموصل وهو يحمل شهادة الدكتوراه في ادارة الاعمال من جامعة المواصل بالعراق وله عدة كتب في الريادة واستراتيجيات الاعمال كما انه قد مجموعة من البحوث في العديد من المؤتمرات مع التركيز على حاضنات رواد الاعمال .</p>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">الدكتورة أمينه بوكيل – الجزائر   </h5>
                <p class="card-text">الدكتورة امينه حاصلة على درجة الدكتوراه في الادب عام ومقارن من جامعة باجي مختار عنابة وتعمل حاليا محاضرة في قسم اللغة والأدب العربي ، كلية الآداب و اللغات ، جامعة جيجل وقد نشرت عدة مقالات وبحوث كما ان له كتابين هما المؤثرات العربية في المقامة العبرية الأندلسية وكتاب تلقي الآخر للثقافة العربية –المقامات أنموذجا وهي تجيد خمس لغات بالاضافة الى اللغة العربية الام .
                </p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                
                <img class="card-img d-block w-100 speaker-img" src="images/13.png" alt="Card image cap">
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                
                <img class="card-img d-block w-100 speaker-img" src="images/Dr Mervet الدكتورة ميرفت.jpg" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">الدكتورة مرفت مسعود جاب الله قطوش – جمهورية مصر العربية   </h5>
                <p class="card-text">
                  الدكتورة مرفت هي صحفية واعلامية وباحثة متخصصة في مجالات تمكين المراة والتسامح والناشئة وقد حصلت على درجة الدكتوراه في الادارب قسم الاجتماع من جامعة عين شمس
                  وتعمل صحفية في جريدة الحوار الحر بالاسكندرية واعلامية معتمدة بمنظمة الضمير العالمي لحقوق الانسان ومسئول لجنة المرأة كما انها محاضرة في مجالات الحوار بين الثقافات كما ان لها
                  عدد من المساهمات البحثية المنشورة والمقدمة في مؤتمرات محلية واقليمية وعالمية
                </p>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div> -->
    
    
  </div>
</div>
</section>
<?php include('include/main_footer.php') ?>