<?php include('include/main_header.php'); ?>



<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4"> المـــؤتمـــــر</h1>
      </div>
    </div>
  </div>
</div>

<section class="conference-banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="conference-BImg">
          <!-- <img src="images/Sports Arabic.jpg" alt=""> -->
          <img src="images/Tolerance_and_Sports.jpg">
        </div>
      </div>
    </div>
  </div>
</section>


<section class="sport-conference-logo">
  <div class="container">
    <div class="row sport-logo">
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/logo europe.jpg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 mb-xs-5">
        <div class="sport-logo-BImg">
          <img src="images/WhatsApp Image 2020-09-13 at 21.23.00 (1).jpeg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/rowad 3.png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/logo YUOI.png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/WhatsApp Image 2020-09-18 at 17.10.32 (2).jpeg" alt="">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="conferences-main mt-5  right-text-class">
  <div class="container">
    
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="jumbotron">
          <!-- <div class="row d-flex justify-content-center">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 d-flex justify-content-center align-items-center">
              <div class="conference-img">
                <img src="images/King.jpg" alt="">
              </div>
            </div>
          </div> -->
          <!-- <h1 class="display-4">Hello, world!</h1> -->
          <p class="lead">يسر الهيئة الدولية للتسامح دعوتكم للمشاركة في مؤتمر التسامح في الرياضة والاعلام الرياضي الذي سيعقد يومي 11 و 12</p>
          <p class="lead"> تيناير  2021 عبر منصة المؤتمرات الافتراضية زوم .
 </p>

          <p class="lead">تتسم المسابقات الرياضية بالجانب العاطفي مما يترتب عليه شحن عاطفي كبير على مستوى اللاعبين والاداريين والمدربين والجمهور وقد افرزت هذه مجموعة من الظواهر غير الصحية في الرياضة وصلت في احيان كثيرة الى العنف في الوقت الذي نعرف جميعا بان الرياضة تخلق الود والالفة .</p>
          <p class="lead">وفي نفس الوقت فان البعض يضع اللوم على الاعلام الرياضي الذي يقوم باشعال فتيل التعصب العاطفي ويؤجج المشاعر والتشدد في المواقف حتى تحول ميدان السباق او الملعب الى ساحة حرب وقتال .</p>
          <p class="lead">الهدف من هذا المؤتمر هو تسليط الضوء على مفاهيم التسامح الايجابي في الرياضة والاعلام الرياضي وأهمية خلق جسور التفاهم والقبول والاحترام بين الجميع وسبل تعزيز التسامح لدى اللاعبين والاداريين والجمهور والمجتمع بشكل عام .</p>
          <p class="lead">يستضيف المؤتمر نخبة ممتازة من الباحثين والمحاضرين من مختلف دول العالم ولذلك فان المحاضرات ستكون باللغتين العربية والانجليزية حيث ستكون هناك حوالي 2 الى 3 جلسات في نفس الوقت بعضها باللغة العربية والبعض باللغة الانجليزية لتتاح لكل مشارك حرية الاختيار بين الجلسات ليس على اساس اللغة فقط ولكن حسب الموضوعات والاهتمامات . </p>
          <!-- <p class="lead"> التسجيل في المؤتمر مجاني بدون اية رسوم والتسجيل مفتوح للجميع في اي مكان في العالم ، وبالنسبة للراغبين في الحصول على شهادة حضور المؤتمر فهناك رسوم بسيطة لاصدار الشهادات .

 </p> --> 
         <!--  <p class="lead">
            الدكتور كينج شيك
          <br> رئيس مجلس امناء الهيئة الدولية للتسامح </p> -->
          <!-- <hr class="my-4">
          <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
          <p class="lead">
            <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
          </p> -->
        </div>
      </div>
    </div>
  </div>
</section>
<section class="home-register pt-5">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <a href="https://docs.google.com/forms/d/e/1FAIpQLSe9lbCiIDTzj5Aorakbj5LIZRUttHtWCI1hLUcPRS5Lux3m7Q/viewform?vc=0&c=0&w=1&flr=0">
        <div class="google-reg">
          <img src="images/reg-400x300.png" alt="">
        </div>
        </a>
      </div>
    </div>
  </div>
</section>
<?php include('include/main_footer.php'); ?>