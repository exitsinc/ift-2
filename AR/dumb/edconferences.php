<?php include('include/main_header.php'); ?>

<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4">  مؤتمر التسامح في التربية والتعليم  <br> 18 – 19 يناير 2021   </h1>
      </div>
    </div>
  </div>
</div>


        <!-- <h1 class="mb-4">  مؤتمر التسامح في التربية والتعليم  <br> 15-16 فبراير 2021</h1> -->




<section class="conference-banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="conference-BImg">
          <!-- <img src="images/Education Arabic.jpg" alt=""> -->
          <img src="images/edconf.jpg" alt="">
        </div>
      </div>
    </div>
  </div>
</section>

<section class="edu-conference-logo">
  <div class="container">
    <div class="row edu-logo">
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/WhatsApp Image 2018-02-12 at 22.05.59 (2).jpeg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 mb-xs-5">
        <div class="edu-logo-BImg">
          <img src="images/GKE Foundation logo with words.jpg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/rowad 3.png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/شعار نبض الامارات (1).png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="edu-logo-BImg">
          <img src="images/WhatsApp Image 2020-09-18 at 17.10.32 (2).jpeg" alt="">
        </div>
      </div>
    </div>
  </div>
</section>


<section class="conferences-main mt-5  right-text-class">
  <div class="container">
    
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="jumbotron">
          <!-- <div class="row d-flex justify-content-center">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 d-flex justify-content-center align-items-center">
              <div class="conference-img">
                <img src="images/King.jpg" alt="">
              </div>
            </div>
          </div> -->
          <!-- <h1 class="display-4">Hello, world!</h1> -->
          <p class="lead">يسر الهيئة الدولية للتسامح والمنظمة الدولية للتسامح دعوتكم للمشاركة في مؤتمر التسامح في التربية والتعليم الذي سيعقد يومي 18 و 19 يناير 2021 على منصة المؤتمرات الافتراضية ( زوم ) . 
 </p>
          <p class="lead">لابد من الاشارة الى اننا نعرف التسامح الايجابي بانه قيمة انسانية وفي نفس الوقت هو عملية مستمرة تركز على بناء الجسور للتفاهم والقبول والاحترام للاخر بغض النظر عن اللون او الجنسية او الديانة او المعتقدات او المكانة الاجتماعية ، ولقد شهد العالم في السنوات الاخيرة هجمات وانتقادات قوية على التعليم والنظم التعليمية وطرح الاباء واولياء الامور تساؤلات عن سلامة الابناء في المدارس في ظل ظواهر التنمر والعنف كما طرحوا تساؤلات حول المحتوى المقدم في المدارس والجامعات .</p>
          <p class="lead">اذا كنا نهتم فعلا بالتنمية المستدامة وخلق مكان آمن للابناء والاحفاد في المستقبل فان علينا الان ان نركز على خلق عالم متسامح حيث ان مسئوليتنا تبدا من ارشاد الابناء للتعرف على والاستمتاع بجمال الطبيعة والورود في الحديقة بدلا من اللهث خلف اصوات المدافع والرشاشات في الالعاب الالكترونية. 

          </p>
          <p class="lead">يهدف هذا المؤتمر الى المساهمة في خلق هذه الجسور </p>
          <p class="lead"> يستضيف المؤتمر نخبة ممتازة من الباحثين والمحاضرين من مختلف دول العالم وستكون هناك محاضرات باللغتين العربية والانجليزية حوالي 2 الى 3 جلسات في نفس الوقت بعضها باللغة العربية والبعض باللغة الانجليزية لتتاح لكل مشارك حرية الاختيار بين الجلسات ليس على اساس اللغة فقط ولكن حسب الموضوعات والاهتمامات ، كما سيتم توزيع جميع اوراق العمل والبحوث في كتيب الكتروني .
 </p>
          <p class="lead"> التسجيل في المؤتمر مجاني بدون اية رسوم ، والتسجيل مفتوح للجميع في اي مكان في العالم ، وبالنسبة للراغبين في الحصول على شهادة حضور المؤتمر فهناك رسوم بسيطة لاصدار الشهادات .

 </p>
          <!-- <p class="lead">Dr. King V Cheek <br> Conference Chair</p> -->
         <!--  <p class="lead">
            الدكتور كينج شيك
          <br> رئيس مجلس امناء الهيئة الدولية للتسامح </p> -->
          <!-- <hr class="my-4">
          <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
          <p class="lead">
            <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
          </p> -->
        </div>
      </div>
    </div>
  </div>
</section>
<section class="home-register pt-5">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <a href="https://docs.google.com/forms/d/e/1FAIpQLScIJwKotyECtOdhEp0Gk7lblSdSh7KPjfyEf3RF8LNGz9Dagg/viewform?vc=0&c=0&w=1&flr=0">
        <div class="google-reg">
          <img src="images/reg-400x300.png" alt="">
        </div>
        </a>
      </div>
    </div>
  </div>
</section>
<?php include('include/main_footer.php'); ?>